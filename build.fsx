// include Fake libs
#r "./packages/FAKE/tools/FakeLib.dll"

open Fake
open Fake.DotNetCli
open Fake.YarnHelper


// Filesets
let appReferences  =
    !! "/src/**/*.csproj"
    ++ "/src/**/*.fsproj"
let buildDirs =
    !! "src/**/bin/"
    -- "**/node_modules/**/bin"
    
// version info
let version = "0.1"  // or retrieve from CI server


Target "Clean" (fun _ ->
    CleanDirs buildDirs
)

Target "YarnRestore" (fun _ ->
   Yarn ( fun p ->
         { p with
             Command = Install Standard
             WorkingDirectory = "./"
         }
   ) |> ignore
   
   Yarn ( fun p ->
         { p with
            Command = Install Standard
            WorkingDirectory = "./src/Annotate.World.WebApp"
         }
   )  |> ignore
)

Target "DotnetRestore" (fun _ ->
    appReferences 
    |> Seq.map (fun project->                     
                    DotNetCli.Restore (fun p-> { p with Project=project })
               )
    |> Seq.toList           
    |> ignore
)

Target "Build" (fun _ ->
    appReferences 
    |> Seq.map (fun project->                     
                    DotNetCli.Build (fun p-> { p with Project=project; Configuration="Debug" })
               )
    |> Seq.toList           
    |> ignore
)



Target "RunWebApp" ( fun () ->    
     (DotNetCli.RunCommand (fun p -> {p with WorkingDir = "./src/Annotate.World.WebApp/"; TimeOut = System.TimeSpan.MaxValue } ) "fable yarn-run start")    
)


// Build order
"Clean"        
    ==> "DotnetRestore"  
    ==> "YarnRestore"
    ==> "Build"
    ==> "RunWebApp"


// start build
RunTargetOrDefault "Build"
