module Annotate.World.Api.Startup

open Suave
open Suave.Successful

open Akkling

type Message =
    | Hi
    | Greet of string

[<EntryPoint>]
let main argv =
    use system = System.create "my-system" <| Configuration.defaultConfig()

    let rec greeter lastKnown = function
        | Hi -> printfn "Who sent Hi? %s?" lastKnown |> ignored
        | Greet(who) ->
            printfn "%s sends greetings" who
            become (greeter who)

    let aref = spawn system "greeter" <| props(actorOf (greeter "Unknown"))

    aref <! Greet "Tom"
    aref <! Greet "Jane"
    aref <! Hi
    
    startWebServer defaultConfig (OK "Hello World!")
    0
