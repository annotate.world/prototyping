module Annotate.World.Geometry
open Annotate.World.ManyList
open System


[<Measure>] type deg
[<Measure>] type rad
[<Measure>] type m

type Point = { x:float<deg>; y:float<deg> }

type Polyline = ManyList<Point>
type Polygon  = ManyList<Point>

type Rectangle = { center: Point; width:float<m>; height:float<m>; rotation: float<rad> }
type Ellipse = { center: Point; width:float<m>; height:float<m>; rotation: float<rad> }

type Geometry =
    | Point of Point    
    | Polyline of Polyline
    | Polygon of Polygon
    | Rectangle of Rectangle
    | Ellipse of Ellipse

