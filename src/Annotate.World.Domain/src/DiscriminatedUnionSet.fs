namespace Annotate.World
open System.Collections
open System.Collections.Generic
open System
open FSharp.Collections
open Fable.Core
open FSharp.Reflection    
    

type DiscriminatedUnionSet<'T> =
    { Values: Map<int,'T>; }

    [<PassGenerics>]
    static member tagReader<'T> (obj:'T) = 
        FSharpValue.GetUnionFields(obj, typeof<'T>) |> fst |> (fun x -> x.Tag)


    interface IEnumerable<'T> with
        member x.GetEnumerator() = 
            (x.Values |> Map.toSeq |> Seq.map snd).GetEnumerator()                
            
    
    interface System.Collections.IEnumerable with
        member x.GetEnumerator() = (x :> _ seq).GetEnumerator() :> IEnumerator


module DiscriminatedUnionSetM =


    let inline private sndCurr _ y = y        
    
    
    [<PassGenerics>]
    let singleton<'T> item : DiscriminatedUnionSet<'T> =                        
        { Values = Map.empty |> Map.add (DiscriminatedUnionSet.tagReader item) item }
    
    [<PassGenerics>]
    let empty<'T> : DiscriminatedUnionSet<'T> =                 
        { Values = Map.empty } 
        
    [<PassGenerics>]    
    let tryAdd item set = 
        let index = DiscriminatedUnionSet.tagReader item
        if set.Values |> Map.containsKey index then
            None
        else
            Some { set with Values = set.Values |> Map.add index item }        

    [<PassGenerics>]    
    let addOrUpdate item set = 
        let index = DiscriminatedUnionSet.tagReader item
        { set with Values = set.Values |> Map.add index item } 
    
    [<PassGenerics>]
    let contains item set = 
        let index = DiscriminatedUnionSet.tagReader item
        set.Values |> Map.containsKey index
    
    [<PassGenerics>]
    let remove item set =         
        { set with Values = set.Values |> Map.remove (DiscriminatedUnionSet.tagReader item) }        
    
    let filter f set = 
        {set with Values = set.Values |> Map.filter (sndCurr>>f)}

    let map f set = 
        {set with Values = set.Values |> Map.map (sndCurr>>f)}

    let forall f set = 
        set.Values |> Map.forall (sndCurr>>f)

    let count set =
        set.Values |> Map.count
    
    let exists f set = 
        set.Values |> Map.exists (sndCurr>>f)

    
    let toSeq set =
        set.Values |> Map.toSeq  |> Seq.map snd
    
    [<PassGenerics>]
    let ofSeq seq =
        let initialState = empty<'T>

        seq |> Seq.fold ( fun set item -> match set |> tryAdd item with
                                            | Some newSet -> newSet
                                            | None -> failwith "This list contains duplicate cases" 
                          ) initialState                     
    let toList set =
        set.Values |> Map.toList |> List.map snd        
    
    [<PassGenerics>]
    let ofList (list : 'T List) : DiscriminatedUnionSet<'T>  = ofSeq list

    [<PassGenerics>]    
    let ofArray (array : 'T []) : DiscriminatedUnionSet<'T>  = ofSeq array

    let toArray set =
        set |> toSeq |> Seq.toArray

    let iter f set = 
        set.Values |> Map.iter (sndCurr>>f)
 
    let fold folder state set = 
        set.Values |> Map.fold (sndCurr>>folder) state

    let foldBack folder state set = 
        set.Values |> Map.foldBack (sndCurr>>folder) state

    let isEmpty set = set.Values.IsEmpty

    let merge set1 set2 = 
        set1.Values |> Map.foldBack Map.add set2.Values