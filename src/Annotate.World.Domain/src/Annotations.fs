module Annotate.World.Annotations
open Microsoft.FSharp.Core
open Geometry
open Annotate.World
open Annotate.World.DiscriminatedUnionSetM

type R = R of byte
type G = G of byte
type B = B of byte 
type A = A of byte

type RGBAColor = R*G*B*A
type RGBColor = R*G*B

let toHex ((R r, G g, B b):RGBColor) =
    sprintf "#%02X%02X%02X" r g b

type Tag = string

type TagSet = Set<Tag>  

type Name = { name: string }

type Annotation = | TagSet of TagSet
                  | Name of Name

type AnnotatedGeometry = { color:RGBColor; feature: Geometry; annotations: DiscriminatedUnionSet<Annotation> }