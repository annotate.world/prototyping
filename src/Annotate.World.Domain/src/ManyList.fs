module public Annotate.World.ManyList
open System.Collections.Generic
open System.Collections

type ManyList<'T> = 
    { List: 'T list }        
        
    member public x.Head = x.List.Head
    
    member public x.Tail = x.List.Tail
    
    member public x.Length = x.List.Length

    interface IEnumerable<'T> with
        member x.GetEnumerator() = 
            (x.List |> List.toSeq).GetEnumerator()                
            
    
    interface System.Collections.IEnumerable with
        member x.GetEnumerator() = (x :> _ seq).GetEnumerator() :> IEnumerator

    

let create fst snd tail = { List = fst::snd::tail }        
let createParamsArray(head,  tail) = { List = head :: List.ofArray tail }        
let inline singleton value = create value []        
let inline head (x: ManyList<_>) = x.Head        
let inline tail (x: ManyList<_>) = x.Tail            
let toList (x: ManyList<_>) = x.List        
let inline length (x: ManyList<_>) = x.Length            
let toArray list = Array.ofList list.List            
let toSeq (list: ManyList<_>) = list.List |> List.toSeq

let ofArray (arr: _ array) =
    match arr.Length with
    | 0 -> invalidArg "arr" "Array is empty"
    | _ -> { List = List.ofArray arr }

let ofList (l: _ list) =
    match l with
    | fst :: snd :: tail -> create fst snd tail
    | _ -> invalidArg "l" "List is empty"

let ofSeq (e: _ seq) =
    if Seq.isEmpty e then
        invalidArg "e" "Sequence is empty"
    else
        {List = List.ofSeq e}

let map f list =
    { List = List.map f list.List }

let cons head tail =
    { List = head :: tail.List }

let appendList list1 list2 =
    { List = list1.List @ list2 }
            
let append list1 list2 =
    { List = list1.List @ list2.List }

let reduce reduction list =
    List.reduce reduction list.List

let last list =
    List.last list.List    

let rev list =
    { List = List.rev list.List}

let collect (mapping:'a -> ManyList<'b>) (list:ManyList<'a>) =
    list.List |> List.collect (fun x -> (mapping x).List) |> ofList

let zip list1 list2 =
    { List = List.zip list1.List list2.List }