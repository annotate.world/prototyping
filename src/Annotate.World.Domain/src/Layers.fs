module Annotate.World.Layers
open System

type Layer = { name:string; purpose: string option; image: string; id: Guid}

type VLayer = 
    | Layer of Layer
    | Group of VLayer list
