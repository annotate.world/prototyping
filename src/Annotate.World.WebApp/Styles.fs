module Annotate.World.WebApp.Styles
open Fable.Helpers.React.Props
open Fable.Core.JsInterop
let annotateBlue = "#1010ff"
let roundedEdgeSize = "5px"

let standardMargin = "10px"
let textStyle : ICSSProp list = 
    [
        CSSProp.FontFamily "Alma Mono, monospace"
        Color annotateBlue                                
    ]

let headerStyle : ICSSProp list =
    textStyle @ [                
        CSSProp.FontSize (!^24.0)
        FontWeight (!^"bold")    
        Margin standardMargin
    ]


let roundedBorderStyle: ICSSProp list =
     [ BorderTopLeftRadius roundedEdgeSize
       BorderTopRightRadius roundedEdgeSize
       BorderBottomLeftRadius roundedEdgeSize
       BorderBottomRightRadius roundedEdgeSize
     ]

let borderStyle: ICSSProp list = 
    roundedBorderStyle @ 
    [ CSSProp.BorderColor annotateBlue 
      BorderWidth "1.2px" 
      BackgroundColor "#FFF"
      BorderStyle "solid"     
    ]