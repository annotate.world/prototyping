module Annotate.World.WebApp.View

open Fable.Helpers.React.Props
module R = Fable.Helpers.React

open Fable.Import.D3


open Annotate.World.Annotations
open Annotate.World.WebApp.Components.Leaflet
open Annotate.World.WebApp.Components.Searchbar
open Annotate.World.Geometry
open Annotate.World.Geometry
open Annotate.World.WebApp.Components.Toolbar
open Annotate.World.WebApp.State
open Annotate.World.WebApp.Components.ToolSelection
open Annotate.World.WebApp.Domain.Tools
open Annotate.World.WebApp.Components.MapOverlay


let GradientOverlay =
    R.div [        
        ClassName "gradient"
        Style [
            PointerEvents "none"
            Position "absolute"
            Left 0
            Top 0
            CSSProp.Width "100%"
            CSSProp.Height "100%"
            ZIndex 999.0            
        ]
    ] []


let sampleDiv = [R.div [Style [CSSProp.Height "150px"; CSSProp.Width "100%"; BackgroundColor "red" ]] []];
let view (model:ProgramState) dispatch =
    let toolSelector = [ ToolSelector 
                            { color = (R 16uy, G 16uy, B 255uy); 
                              currentlySelectedTool = model.toolState.currentTool
                              onToolSelect = fun tool -> dispatch <| SetTool tool
                            }
                       ]
    let toolbars =  
         ToolbarContainer [ 
             (Toolbar { title = "tools"; opened = model.toolState.toolPanelVisibility; onVisiblityToggled = (fun opened -> (dispatch (ToggleToolPanelVisibility opened))) } toolSelector)                            
             (Toolbar { title = "layers"; opened = model.layerState.layerPanelVisibility; onVisiblityToggled = (fun opened -> (dispatch (ToggleLayerPanelVisibility opened))) } sampleDiv)
         ]

    R.div [ Style [CSSProp.Width "100%"; CSSProp.Height "100%"]] 
          [
            Map { zoom = 4.0; center = { x = -96.9<deg>; y = 37.8<deg> }; overlays = [MapCircleOverlay {circles = [|(-96.9, 37.8); (-90.9, 30.8)|]}]}
            GradientOverlay
            Searchbar { currentText = (model.searchState.term |> Option.defaultValue ""); onChange = (fun str -> dispatch (SetSearchTerm str)) }            
            toolbars             
          ] 