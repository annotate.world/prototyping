module Annotate.World.WebApp.State

open Elmish
open Annotate.World.Geometry
open Annotate.World.WebApp.Domain.Tools

type SearchState = { term : string option }
type ToolState = { toolPanelVisibility : bool; currentTool: Tool }
type LayerState = { layerPanelVisibility : bool }

type ProgramState = { zoom:float 
                      center : Point
                      searchState : SearchState 
                      toolState : ToolState 
                      layerState : LayerState  
                     }

type ProgramEvent = | SetSearchTerm of string                    
                    | ToggleToolPanelVisibility of bool
                    | ToggleLayerPanelVisibility of bool

                    | SetTool of Tool

let initialState () =
    let point = { x = -96.9<deg>; y = 37.8<deg> } 
    let searchState = { term = None }
    { zoom=18.0; 
      center = point
      searchState = searchState
      toolState = { toolPanelVisibility = false; currentTool = SelectTool } 
      layerState = { layerPanelVisibility = false } 
    } , Cmd.none

    
let update evnt prev =
    match evnt with
    | SetSearchTerm str ->
        if isNull str || str = "" then 
            { prev with searchState = { term = None } }, Cmd.none
        else 
            { prev with searchState = { term = Some str } }, Cmd.none
    | ToggleToolPanelVisibility b ->  { prev with toolState  = { prev.toolState with toolPanelVisibility = b } }, Cmd.none
    | ToggleLayerPanelVisibility b -> { prev with layerState = { prev.layerState with layerPanelVisibility = b} }, Cmd.none
    | SetTool t ->  { prev with toolState = {prev.toolState with currentTool = t }}, Cmd.none


