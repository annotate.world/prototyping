module Annotate.World.WebApp.Domain.Tools

type Tool =     
    | SelectTool
    | RulerTool    
    | PointTool
    | LineTool
    | RectangleTool
    | ShapeTool    
    | CircleTool
