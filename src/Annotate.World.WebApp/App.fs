module Annotate.World.WebApp.Startup
open Fable.Core
open Elmish
open Elmish.React


Program.mkProgram State.initialState State.update View.view
|> Program.withReact "root"
|> Program.withConsoleTrace
|> Program.run