namespace Fable.Import
open System
open System.Text.RegularExpressions
open Fable.Core
open Fable.Import.JS


module GeoJSON =

    type [<AllowNullLiteral>] GeoJsonObject =
        abstract ``type``: string with get, set
        abstract bbox: ResizeArray<float> option with get, set
        abstract crs: CoordinateReferenceSystem option with get, set

    and Position = ResizeArray<float>

    and [<AllowNullLiteral>] DirectGeometryObject =
        inherit GeoJsonObject
        abstract coordinates: U4<ResizeArray<ResizeArray<ResizeArray<Position>>>, ResizeArray<ResizeArray<Position>>, ResizeArray<Position>, Position> with get, set

    and GeometryObject = U2<DirectGeometryObject, GeometryCollection>

    and [<AllowNullLiteral>] Point =
        inherit DirectGeometryObject
        abstract ``type``: obj with get, set
        abstract coordinates: Position with get, set

    and [<AllowNullLiteral>] MultiPoint =
        inherit DirectGeometryObject
        abstract ``type``: obj with get, set
        abstract coordinates: ResizeArray<Position> with get, set

    and [<AllowNullLiteral>] LineString =
        inherit DirectGeometryObject
        abstract ``type``: obj with get, set
        abstract coordinates: ResizeArray<Position> with get, set

    and [<AllowNullLiteral>] MultiLineString =
        inherit DirectGeometryObject
        abstract ``type``: obj with get, set
        abstract coordinates: ResizeArray<ResizeArray<Position>> with get, set

    and [<AllowNullLiteral>] Polygon =
        inherit DirectGeometryObject
        abstract ``type``: obj with get, set
        abstract coordinates: ResizeArray<ResizeArray<Position>> with get, set

    and [<AllowNullLiteral>] MultiPolygon =
        inherit DirectGeometryObject
        abstract ``type``: obj with get, set
        abstract coordinates: ResizeArray<ResizeArray<ResizeArray<Position>>> with get, set

    and [<AllowNullLiteral>] GeometryCollection =
        inherit GeoJsonObject
        abstract ``type``: obj with get, set
        abstract geometries: ResizeArray<GeometryObject> with get, set

    and [<AllowNullLiteral>] Feature<'T> =
        inherit GeoJsonObject
        abstract ``type``: obj with get, set
        abstract geometry: 'T with get, set
        abstract properties: U2<obj, obj> with get, set
        abstract ``null``: obj with get, set
        abstract id: string option with get, set

    and [<AllowNullLiteral>] FeatureCollection<'T> =
        inherit GeoJsonObject
        abstract ``type``: obj with get, set
        abstract features: ResizeArray<Feature<'T>> with get, set

    and [<AllowNullLiteral>] CoordinateReferenceSystem =
        abstract ``type``: string with get, set
        abstract properties: obj with get, set

    and [<AllowNullLiteral>] NamedCoordinateReferenceSystem =
        inherit CoordinateReferenceSystem
        abstract properties: obj with get, set

    and [<AllowNullLiteral>] LinkedCoordinateReferenceSystem =
        inherit CoordinateReferenceSystem
        abstract properties: obj with get, set

