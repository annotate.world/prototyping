namespace Fable.Import
open System
open System.Text.RegularExpressions
open Fable.Core
open Fable.Import.JS
open Fable.Import.Browser

module Leaflet =

    type NativeMouseEvent = MouseEvent

    and NativeKeyboardEvent = KeyboardEvent

    and [<AllowNullLiteral>] [<Import("Class","leaflet")>] Class() =
        static member extend(props: obj): obj = jsNative
        static member ``include``(props: obj): obj = jsNative
        static member mergeOptions(props: obj): obj = jsNative
        static member addInitHook(initHookFn: Func<unit, unit>): obj = jsNative

    and [<AllowNullLiteral>] [<Import("Transformation","leaflet")>] Transformation(a: float, b: float, c: float, d: float) =
        member __.transform(point: Point, ?scale: float): Point = jsNative
        member __.untransform(point: Point, ?scale: float): Point = jsNative

    and [<AllowNullLiteral>] [<Import("DomUtil","leaflet")>] DomUtil() =
        static member get(element: U2<string, HTMLElement>): HTMLElement = jsNative
        static member getStyle(el: HTMLElement, styleAttrib: string): string = jsNative
        static member create(tagName: string, ?className: string, ?container: HTMLElement): HTMLElement = jsNative
        static member remove(el: HTMLElement): unit = jsNative
        static member empty(el: HTMLElement): unit = jsNative
        static member toFront(el: HTMLElement): unit = jsNative
        static member toBack(el: HTMLElement): unit = jsNative
        static member hasClass(el: HTMLElement, name: string): bool = jsNative
        static member addClass(el: HTMLElement, name: string): unit = jsNative
        static member removeClass(el: HTMLElement, name: string): unit = jsNative
        static member setClass(el: HTMLElement, name: string): unit = jsNative
        static member getClass(el: HTMLElement): string = jsNative
        static member setOpacity(el: HTMLElement, opacity: float): unit = jsNative
        static member testProp(props: ResizeArray<string>): U2<string, bool> = jsNative
        static member setTransform(el: HTMLElement, offset: Point, ?scale: float): unit = jsNative
        static member setPosition(el: HTMLElement, position: Point): unit = jsNative
        static member getPosition(el: HTMLElement): Point = jsNative
        static member disableTextSelection(): unit = jsNative
        static member enableTextSelection(): unit = jsNative
        static member disableImageDrag(): unit = jsNative
        static member enableImageDrag(): unit = jsNative
        static member preventOutline(el: HTMLElement): unit = jsNative
        static member restoreOutline(): unit = jsNative

    and [<AllowNullLiteral>] [<Import("CRS","leaflet")>] CRS() =
        member __.code with get(): string = jsNative and set(v: string): unit = jsNative
        member __.wrapLng with get(): float * float = jsNative and set(v: float * float): unit = jsNative
        member __.wrapLat with get(): float * float = jsNative and set(v: float * float): unit = jsNative
        member __.infinite with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.latLngToPoint(latlng: LatLngExpression, zoom: float): Point = jsNative
        member __.pointToLatLng(point: PointExpression, zoom: float): LatLng = jsNative
        member __.project(latlng: LatLngExpression): Point = jsNative
        member __.unproject(point: PointExpression): LatLng = jsNative
        member __.scale(zoom: float): float = jsNative
        member __.zoom(scale: float): float = jsNative
        member __.getProjectedBounds(zoom: float): Bounds = jsNative
        member __.distance(latlng1: LatLngExpression, latlng2: LatLngExpression): float = jsNative
        member __.wrapLatLng(latlng: LatLngExpression): LatLng = jsNative

    and [<AllowNullLiteral>] Projection =
        abstract bounds: LatLngBounds with get, set
        abstract project: latlng: LatLngExpression -> Point
        abstract unproject: point: PointExpression -> LatLng

    and [<AllowNullLiteral>] [<Import("LatLng","leaflet")>] LatLng(coords: U4<LatLngTuple, float * float * float, LatLngLiteral, obj>) =
        member __.lat with get(): float = jsNative and set(v: float): unit = jsNative
        member __.lng with get(): float = jsNative and set(v: float): unit = jsNative
        member __.alt with get(): float = jsNative and set(v: float): unit = jsNative
        member __.equals(otherLatLng: LatLngExpression, ?maxMargin: float): bool = jsNative
        member __.toString(): string = jsNative
        member __.distanceTo(otherLatLng: LatLngExpression): float = jsNative
        member __.wrap(): LatLng = jsNative
        member __.toBounds(sizeInMeters: float): LatLngBounds = jsNative

    and [<AllowNullLiteral>] LatLngLiteral =
        abstract lat: float with get, set
        abstract lng: float with get, set

    and LatLngTuple = float * float

    and LatLngExpression = U3<LatLng, LatLngLiteral, LatLngTuple>

    and [<AllowNullLiteral>] [<Import("LatLngBounds","leaflet")>] LatLngBounds(latlngs: LatLngBoundsLiteral) =
        member __.extend(latlngOrBounds: U2<LatLngExpression, LatLngBoundsExpression>): obj = jsNative
        member __.pad(bufferRatio: float): LatLngBounds = jsNative
        member __.getCenter(): LatLng = jsNative
        member __.getSouthWest(): LatLng = jsNative
        member __.getNorthEast(): LatLng = jsNative
        member __.getNorthWest(): LatLng = jsNative
        member __.getSouthEast(): LatLng = jsNative
        member __.getWest(): float = jsNative
        member __.getSouth(): float = jsNative
        member __.getEast(): float = jsNative
        member __.getNorth(): float = jsNative
        member __.contains(otherBoundsOrLatLng: U2<LatLngBoundsExpression, LatLngExpression>): bool = jsNative
        member __.intersects(otherBounds: LatLngBoundsExpression): bool = jsNative
        member __.overlaps(otherBounds: BoundsExpression): bool = jsNative
        member __.toBBoxString(): string = jsNative
        member __.equals(otherBounds: LatLngBoundsExpression): bool = jsNative
        member __.isValid(): bool = jsNative

    and LatLngBoundsLiteral = ResizeArray<LatLngTuple>

    and LatLngBoundsExpression = U2<LatLngBounds, LatLngBoundsLiteral>

    and PointTuple = float * float

    and [<AllowNullLiteral>] [<Import("Point","leaflet")>] Point(coords: U2<PointTuple, obj>) =
        member __.x with get(): float = jsNative and set(v: float): unit = jsNative
        member __.y with get(): float = jsNative and set(v: float): unit = jsNative
        member __.clone(): Point = jsNative
        member __.add(otherPoint: PointExpression): Point = jsNative
        member __.subtract(otherPoint: PointExpression): Point = jsNative
        member __.divideBy(num: float): Point = jsNative
        member __.multiplyBy(num: float): Point = jsNative
        member __.scaleBy(scale: PointExpression): Point = jsNative
        member __.unscaleBy(scale: PointExpression): Point = jsNative
        member __.round(): Point = jsNative
        member __.floor(): Point = jsNative
        member __.ceil(): Point = jsNative
        member __.distanceTo(otherPoint: PointExpression): float = jsNative
        member __.equals(otherPoint: PointExpression): bool = jsNative
        member __.contains(otherPoint: PointExpression): bool = jsNative
        member __.toString(): string = jsNative

    and PointExpression = U2<Point, PointTuple>

    and BoundsLiteral = PointTuple * PointTuple

    and [<AllowNullLiteral>] [<Import("Bounds","leaflet")>] Bounds(points: U2<ResizeArray<Point>, BoundsLiteral>) =
        member __.min with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.max with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.extend(point: PointExpression): obj = jsNative
        member __.getCenter(?round: bool): Point = jsNative
        member __.getBottomLeft(): Point = jsNative
        member __.getTopRight(): Point = jsNative
        member __.getSize(): Point = jsNative
        member __.contains(pointOrBounds: U2<BoundsExpression, PointExpression>): bool = jsNative
        member __.intersects(otherBounds: BoundsExpression): bool = jsNative
        member __.overlaps(otherBounds: BoundsExpression): bool = jsNative

    and BoundsExpression = U2<Bounds, BoundsLiteral>

    and EventHandlerFn = Func<Event, unit>

    and [<AllowNullLiteral>] EventHandlerFnMap =
        [<Emit("$0[$1]{{=$2}}")>] abstract Item: ``type``: string -> EventHandlerFn with get, set

    and [<AllowNullLiteral>] [<Import("Evented","leaflet")>] Evented() =
        inherit Class()
        member __.on(``type``: string, fn: EventHandlerFn, ?context: obj): obj = jsNative
        [<Emit("$0.on('click', $1)")>] member __.on_click(fn: EventHandlerFn): obj = jsNative
        member __.on(eventMap: EventHandlerFnMap): obj = jsNative
        member __.off(``type``: string, ?fn: EventHandlerFn, ?context: obj): obj = jsNative
        member __.off(eventMap: EventHandlerFnMap): obj = jsNative
        member __.off(): obj = jsNative
        member __.fire(``type``: string, ?data: obj, ?propagate: bool): obj = jsNative
        member __.listens(``type``: string): bool = jsNative
        member __.once(``type``: string, fn: EventHandlerFn, ?context: obj): obj = jsNative
        member __.once(eventMap: EventHandlerFnMap): obj = jsNative
        member __.addEventParent(obj: Evented): obj = jsNative
        member __.removeEventParent(obj: Evented): obj = jsNative
        member __.addEventListener(``type``: string, fn: EventHandlerFn, ?context: obj): obj = jsNative
        member __.addEventListener(eventMap: EventHandlerFnMap): obj = jsNative
        member __.removeEventListener(``type``: string, fn: EventHandlerFn, ?context: obj): obj = jsNative
        member __.removeEventListener(eventMap: EventHandlerFnMap): obj = jsNative
        member __.clearAllEventListeners(): obj = jsNative
        member __.addOneTimeEventListener(``type``: string, fn: EventHandlerFn, ?context: obj): obj = jsNative
        member __.addOneTimeEventListener(eventMap: EventHandlerFnMap): obj = jsNative
        member __.fireEvent(``type``: string, ?data: obj, ?propagate: bool): obj = jsNative
        member __.hasEventListeners(``type``: string): bool = jsNative

    and [<AllowNullLiteral>] [<Import("Draggable","leaflet")>] Draggable(element: HTMLElement, ?dragStartTarget: HTMLElement, ?preventOutline: bool) =
        inherit Evented()
        member __.enable(): unit = jsNative
        member __.disable(): unit = jsNative
        member __.finishDrag(): unit = jsNative

    and [<AllowNullLiteral>] LayerOptions =
        abstract pane: string option with get, set

    and [<AllowNullLiteral>] InteractiveLayerOptions =
        inherit LayerOptions
        abstract interactive: bool option with get, set

    and [<AllowNullLiteral>] [<Import("Layer","leaflet")>] Layer(?options: LayerOptions) =
        inherit Evented()
        member __.addTo(map: Map): obj = jsNative
        member __.remove(): obj = jsNative
        member __.removeFrom(map: Map): obj = jsNative
        member __.getPane(?name: string): HTMLElement = jsNative
        member __.bindPopup(content: U3<Func<Layer, Content>, Content, Popup>, ?options: PopupOptions): obj = jsNative
        member __.unbindPopup(): obj = jsNative
        member __.openPopup(?latlng: LatLngExpression): obj = jsNative
        member __.closePopup(): obj = jsNative
        member __.togglePopup(): obj = jsNative
        member __.isPopupOpen(): bool = jsNative
        member __.setPopupContent(content: U2<Content, Popup>): obj = jsNative
        member __.getPopup(): Popup = jsNative
        member __.bindTooltip(content: U3<Func<Layer, Content>, Tooltip, Content>, ?options: TooltipOptions): obj = jsNative
        member __.unbindTooltip(): obj = jsNative
        member __.openTooltip(?latlng: LatLngExpression): obj = jsNative
        member __.closeTooltip(): obj = jsNative
        member __.toggleTooltip(): obj = jsNative
        member __.isTooltipOpen(): bool = jsNative
        member __.setTooltipContent(content: U2<Content, Tooltip>): obj = jsNative
        member __.getTooltip(): Tooltip = jsNative
        member __.onAdd(map: Map): obj = jsNative
        member __.onRemove(map: Map): obj = jsNative
        member __.getEvents(): obj = jsNative
        member __.getAttribution(): string = jsNative
        member __.beforeAdd(map: Map): obj = jsNative

    and [<AllowNullLiteral>] GridLayerOptions = 
        abstract tileSize: U2<float, Point> option with get, set 
        abstract opacity: float option with get, set 
        abstract updateWhenIdle: bool option with get, set 
        abstract updateWhenZooming: bool option with get, set 
        abstract updateInterval: float option with get, set 
        abstract attribution: string option with get, set 
        abstract zIndex: float option with get, set 
        abstract bounds: LatLngBoundsExpression option with get, set 
        abstract minZoom: float option with get, set 
        abstract maxZoom: float option with get, set 
        abstract noWrap: bool option with get, set 
        abstract pane: string option with get, set 
        abstract className: string option with get, set 
        abstract keepBuffer: float option with get, set 


    and [<AllowNullLiteral>] [<Import("GridLayer","leaflet")>] GridLayer(?options: GridLayerOptions) =
        inherit Layer()
        member __.bringToFront(): obj = jsNative
        member __.bringToBack(): obj = jsNative
        member __.getAttribution(): string = jsNative
        member __.getContainer(): HTMLElement = jsNative
        member __.setOpacity(opacity: float): obj = jsNative
        member __.setZIndex(zIndex: float): obj = jsNative
        member __.isLoading(): bool = jsNative
        member __.redraw(): obj = jsNative
        member __.getTileSize(): Point = jsNative

    and [<AllowNullLiteral>] TileLayerOptions = 
        inherit GridLayerOptions 
        abstract minZoom: float option with get, set 
        abstract maxZoom: float option with get, set 
        abstract maxNativeZoom: float option with get, set 
        abstract minNativeZoom: float option with get, set 
        abstract subdomains: U2<string, ResizeArray<string>> option with get, set 
        abstract errorTileUrl: string option with get, set 
        abstract zoomOffset: float option with get, set 
        abstract tms: bool option with get, set 
        abstract zoomReverse: bool option with get, set 
        abstract detectRetina: bool option with get, set 
        abstract crossOrigin: bool option with get, set 
        [<Emit("$0[$1]{{=$2}}")>] abstract Item: name: string -> obj with get, set 

                


    and [<AllowNullLiteral>] [<Import("TileLayer","leaflet")>] TileLayer(urlTemplate: string, ?options: TileLayerOptions) =
        inherit GridLayer()
        member __.options with get(): TileLayerOptions = jsNative and set(v: TileLayerOptions): unit = jsNative
        member __.setUrl(url: string, ?noRedraw: bool): obj = jsNative

    and [<AllowNullLiteral>] WMSOptions = 
        inherit TileLayerOptions 
        abstract layers: string with get, set 
        abstract styles: string option with get, set 
        abstract format: string option with get, set 
        abstract transparent: bool option with get, set 
        abstract version: string option with get, set 
        abstract crs: CRS option with get, set 
        abstract uppercase: bool option with get, set 

    and [<AllowNullLiteral>] WMSParams =
        abstract format: string option with get, set
        abstract layers: string with get, set
        abstract request: string option with get, set
        abstract service: string option with get, set
        abstract styles: string option with get, set
        abstract version: string option with get, set
        abstract transparent: bool option with get, set
        abstract width: float option with get, set
        abstract height: float option with get, set

    and [<AllowNullLiteral>] ImageOverlayOptions =
        inherit LayerOptions
        abstract opacity: float option with get, set
        abstract alt: string option with get, set
        abstract interactive: bool option with get, set
        abstract attribution: string option with get, set
        abstract crossOrigin: bool option with get, set

    and [<AllowNullLiteral>] [<Import("ImageOverlay","leaflet")>] ImageOverlay(imageUrl: string, bounds: LatLngBoundsExpression, ?options: ImageOverlayOptions) =
        inherit Layer()
        member __.options with get(): ImageOverlayOptions = jsNative and set(v: ImageOverlayOptions): unit = jsNative
        member __.setOpacity(opacity: float): obj = jsNative
        member __.bringToFront(): obj = jsNative
        member __.bringToBack(): obj = jsNative
        member __.setUrl(url: string): obj = jsNative
        member __.setBounds(bounds: LatLngBounds): obj = jsNative
        member __.getBounds(): LatLngBounds = jsNative
        member __.getElement(): HTMLImageElement = jsNative

    and [<StringEnum>] LineCapShape =
        | Butt
        | Round
        | Square
        | Inherit

    and [<StringEnum>] LineJoinShape =
        | Miter
        | Round
        | Bevel
        | Inherit

    and [<StringEnum>] FillRule =
        | Nonzero
        | Evenodd
        | Inherit

    and [<AllowNullLiteral>] PathOptions =
        inherit InteractiveLayerOptions
        abstract stroke: bool option with get, set
        abstract color: string option with get, set
        abstract weight: float option with get, set
        abstract opacity: float option with get, set
        abstract lineCap: LineCapShape option with get, set
        abstract lineJoin: LineJoinShape option with get, set
        abstract dashArray: string option with get, set
        abstract dashOffset: string option with get, set
        abstract fill: bool option with get, set
        abstract fillColor: string option with get, set
        abstract fillOpacity: float option with get, set
        abstract fillRule: FillRule option with get, set
        abstract renderer: Renderer option with get, set
        abstract className: string option with get, set

    and [<AllowNullLiteral>] [<Import("Path","leaflet")>] Path() =
        inherit Layer()
        member __.options with get(): PathOptions = jsNative and set(v: PathOptions): unit = jsNative
        member __.redraw(): obj = jsNative
        member __.setStyle(style: PathOptions): obj = jsNative
        member __.bringToFront(): obj = jsNative
        member __.bringToBack(): obj = jsNative
        member __.getElement(): HTMLElement = jsNative

    and [<AllowNullLiteral>] PolylineOptions =
        inherit PathOptions
        abstract smoothFactor: float option with get, set
        abstract noClip: bool option with get, set

    and [<AllowNullLiteral>] [<Import("InternalPolyline","leaflet")>] InternalPolyline() =
        inherit Path()
        member __.options with get(): PolylineOptions = jsNative and set(v: PolylineOptions): unit = jsNative
        member __.getLatLngs(): ResizeArray<LatLng> = jsNative
        member __.setLatLngs(latlngs: ResizeArray<LatLngExpression>): obj = jsNative
        member __.isEmpty(): bool = jsNative
        member __.getCenter(): LatLng = jsNative
        member __.getBounds(): LatLngBounds = jsNative
        member __.addLatLng(latlng: U2<LatLngExpression, ResizeArray<LatLngExpression>>): obj = jsNative

    and [<AllowNullLiteral>] [<Import("Polyline","leaflet")>] Polyline(latlngs: ResizeArray<LatLngExpression>, ?options: PolylineOptions) =
        inherit InternalPolyline()
        member __.feature with get(): GeoJSON.Feature<U2<GeoJSON.LineString, GeoJSON.MultiLineString>> = jsNative and set(v: GeoJSON.Feature<U2<GeoJSON.LineString, GeoJSON.MultiLineString>>): unit = jsNative
        member __.toGeoJSON(): GeoJSON.Feature<U2<GeoJSON.LineString, GeoJSON.MultiLineString>> = jsNative

    and [<AllowNullLiteral>] [<Import("Polygon","leaflet")>] Polygon(latlngs: ResizeArray<LatLngExpression>, ?options: PolylineOptions) =
        inherit InternalPolyline()
        member __.feature with get(): GeoJSON.Feature<U2<GeoJSON.Polygon, GeoJSON.MultiPolygon>> = jsNative and set(v: GeoJSON.Feature<U2<GeoJSON.Polygon, GeoJSON.MultiPolygon>>): unit = jsNative
        member __.toGeoJSON(): GeoJSON.Feature<U2<GeoJSON.Polygon, GeoJSON.MultiPolygon>> = jsNative

    and [<AllowNullLiteral>] [<Import("Rectangle","leaflet")>] Rectangle(latLngBounds: LatLngBoundsExpression, ?options: PolylineOptions) =
        //inherit Polygon()
        member __.setBounds(latLngBounds: LatLngBoundsExpression): obj = jsNative

    and [<AllowNullLiteral>] CircleMarkerOptions =
        inherit PathOptions
        abstract radius: float option with get, set

    and [<AllowNullLiteral>] [<Import("CircleMarker","leaflet")>] CircleMarker(latlng: LatLngExpression, ?options: CircleMarkerOptions) =
        inherit Path()
        member __.options with get(): CircleMarkerOptions = jsNative and set(v: CircleMarkerOptions): unit = jsNative
        member __.feature with get(): GeoJSON.Feature<GeoJSON.Point> = jsNative and set(v: GeoJSON.Feature<GeoJSON.Point>): unit = jsNative
        member __.toGeoJSON(): GeoJSON.Feature<GeoJSON.Point> = jsNative
        member __.setLatLng(latLng: LatLngExpression): obj = jsNative
        member __.getLatLng(): LatLng = jsNative
        member __.setRadius(radius: float): obj = jsNative
        member __.getRadius(): float = jsNative

    and [<AllowNullLiteral>] [<Import("Circle","leaflet")>] Circle(latlng: LatLngExpression, radius: float, ?options: CircleMarkerOptions) =
        inherit CircleMarker(latlng, ?options = options)
        member __.getBounds(): LatLngBounds = jsNative

    and [<AllowNullLiteral>] RendererOptions =
        inherit LayerOptions
        abstract padding: float option with get, set

    and [<AllowNullLiteral>] [<Import("Renderer","leaflet")>] Renderer(?options: RendererOptions) =
        inherit Layer()
        member __.options with get(): RendererOptions = jsNative and set(v: RendererOptions): unit = jsNative

    and [<AllowNullLiteral>] [<Import("SVG","leaflet")>] SVG() =
        inherit Renderer()


    and [<AllowNullLiteral>] [<Import("Canvas","leaflet")>] Canvas() =
        inherit Renderer()


    and [<AllowNullLiteral>] [<Import("LayerGroup","leaflet")>] LayerGroup(layers: ResizeArray<Layer>) =
        inherit Layer()
        member __.feature with get(): U3<GeoJSON.FeatureCollection<GeoJSON.GeometryObject>, GeoJSON.Feature<GeoJSON.MultiPoint>, GeoJSON.GeometryCollection> = jsNative and set(v: U3<GeoJSON.FeatureCollection<GeoJSON.GeometryObject>, GeoJSON.Feature<GeoJSON.MultiPoint>, GeoJSON.GeometryCollection>): unit = jsNative
        member __.toGeoJSON(): U3<GeoJSON.FeatureCollection<GeoJSON.GeometryObject>, GeoJSON.Feature<GeoJSON.MultiPoint>, GeoJSON.GeometryCollection> = jsNative
        member __.addLayer(layer: Layer): obj = jsNative
        member __.removeLayer(layer: U2<float, Layer>): obj = jsNative
        member __.hasLayer(layer: Layer): bool = jsNative
        member __.clearLayers(): obj = jsNative
        member __.invoke(methodName: string, [<ParamArray>] ``params``: obj[]): obj = jsNative
        member __.eachLayer(fn: Func<Layer, unit>, ?context: obj): obj = jsNative
        member __.getLayer(id: float): Layer = jsNative
        member __.getLayers(): ResizeArray<Layer> = jsNative
        member __.setZIndex(zIndex: float): obj = jsNative
        member __.getLayerId(layer: Layer): float = jsNative

    and [<AllowNullLiteral>] [<Import("FeatureGroup","leaflet")>] FeatureGroup() =
        // inherit LayerGroup()
        member __.setStyle(style: PathOptions): obj = jsNative
        member __.bringToFront(): obj = jsNative
        member __.bringToBack(): obj = jsNative
        member __.getBounds(): LatLngBounds = jsNative

    and StyleFunction = Func<GeoJSON.Feature<GeoJSON.GeometryObject>, PathOptions>

    and [<AllowNullLiteral>] GeoJSONOptions =
        inherit LayerOptions
        abstract pointToLayer: Func<GeoJSON.Feature<GeoJSON.Point>, LatLng, Layer> option with get, set
        abstract style: StyleFunction option with get, set
        abstract onEachFeature: Func<GeoJSON.Feature<GeoJSON.GeometryObject>, Layer, unit> option with get, set
        abstract filter: Func<GeoJSON.Feature<GeoJSON.GeometryObject>, bool> option with get, set
        abstract coordsToLatLng: Func<U2<float * float, float * float * float>, LatLng> option with get, set

    and [<AllowNullLiteral>] [<Import("GeoJSON","leaflet")>] GeoJSON(?geojson: GeoJSON.GeoJsonObject, ?options: GeoJSONOptions) =
        inherit FeatureGroup()
        member __.options with get(): GeoJSONOptions = jsNative and set(v: GeoJSONOptions): unit = jsNative
        static member geometryToLayer(featureData: GeoJSON.Feature<GeoJSON.GeometryObject>, ?options: GeoJSONOptions): Layer = jsNative
        static member coordsToLatLng(coords: U2<float * float, float * float * float>): LatLng = jsNative
        static member coordsToLatLngs(coords: ResizeArray<obj>, ?levelsDeep: float, ?coordsToLatLng: Func<U2<float * float, float * float * float>, LatLng>): ResizeArray<obj> = jsNative
        static member latLngToCoords(latlng: LatLng): float * float * float = jsNative
        static member latLngsToCoords(latlngs: ResizeArray<obj>, ?levelsDeep: float, ?closed: bool): ResizeArray<obj> = jsNative
        static member asFeature(geojson: U2<GeoJSON.Feature<GeoJSON.GeometryObject>, GeoJSON.GeometryObject>): GeoJSON.Feature<GeoJSON.GeometryObject> = jsNative
        member __.addData(data: GeoJSON.GeoJsonObject): Layer = jsNative
        member __.resetStyle(layer: Layer): Layer = jsNative
        member __.setStyle(style: StyleFunction): obj = jsNative

    and Zoom = U2<bool, obj>

    and [<AllowNullLiteral>] MapOptions = 
        abstract preferCanvas: bool option with get, set 
        abstract attributionControl: bool option with get, set 
        abstract zoomControl: bool option with get, set 
        abstract closePopupOnClick: bool option with get, set 
        abstract zoomSnap: float option with get, set 
        abstract zoomDelta: float option with get, set 
        abstract trackResize: bool option with get, set 
        abstract boxZoom: bool option with get, set 
        abstract doubleClickZoom: Zoom option with get, set 
        abstract dragging: bool option with get, set 
        abstract crs: CRS option with get, set 
        abstract center: LatLngExpression option with get, set 
        abstract zoom: float option with get, set 
        abstract minZoom: float option with get, set 
        abstract maxZoom: float option with get, set 
        abstract layers: ResizeArray<Layer> option with get, set 
        abstract maxBounds: LatLngBoundsExpression option with get, set 
        abstract renderer: Renderer option with get, set 
        abstract fadeAnimation: bool option with get, set 
        abstract markerZoomAnimation: bool option with get, set 
        abstract transform3DLimit: float option with get, set 
        abstract zoomAnimation: bool option with get, set 
        abstract zoomAnimationThreshold: float option with get, set 
        abstract inertia: bool option with get, set 
        abstract inertiaDeceleration: float option with get, set 
        abstract inertiaMaxSpeed: float option with get, set 
        abstract easeLinearity: float option with get, set 
        abstract worldCopyJump: bool option with get, set 
        abstract maxBoundsViscosity: float option with get, set 
        abstract keyboard: bool option with get, set 
        abstract keyboardPanDelta: float option with get, set 
        abstract scrollWheelZoom: Zoom option with get, set 
        abstract wheelDebounceTime: float option with get, set 
        abstract wheelPxPerZoomLevel: float option with get, set 
        abstract tap: bool option with get, set 
        abstract tapTolerance: float option with get, set 
        abstract touchZoom: Zoom option with get, set 
        abstract bounceAtZoomLLimits: bool option with get, set 

    and [<StringEnum>] ControlPosition =
        | Topleft
        | Topright
        | Bottomleft
        | Bottomright

    and [<AllowNullLiteral>] ControlOptions =
        abstract position: ControlPosition option with get, set

    and [<AllowNullLiteral>] [<Import("Control","leaflet")>] Control(?options: ControlOptions) =
        inherit Class()
        member __.options with get(): ControlOptions = jsNative and set(v: ControlOptions): unit = jsNative
        member __.getPosition(): ControlPosition = jsNative
        member __.setPosition(position: ControlPosition): obj = jsNative
        member __.getContainer(): HTMLElement = jsNative
        member __.addTo(map: Map): obj = jsNative
        member __.remove(): obj = jsNative
        member __.onAdd(map: Map): HTMLElement = jsNative
        member __.onRemove(map: Map): unit = jsNative

    and [<AllowNullLiteral>] DivOverlayOptions =
        abstract offset: PointExpression option with get, set
        abstract zoomAnimation: bool option with get, set
        abstract className: string option with get, set
        abstract pane: string option with get, set

    and [<AllowNullLiteral>] PopupOptions =
        inherit DivOverlayOptions
        abstract maxWidth: float option with get, set
        abstract minWidth: float option with get, set
        abstract maxHeight: float option with get, set
        abstract autoPan: bool option with get, set
        abstract autoPanPaddingTopLeft: PointExpression option with get, set
        abstract autoPanPaddingBottomRight: PointExpression option with get, set
        abstract autoPanPadding: PointExpression option with get, set
        abstract keepInView: bool option with get, set
        abstract closeButton: bool option with get, set
        abstract autoClose: bool option with get, set
        abstract closeOnClick: bool option with get, set

    and Content = U2<string, HTMLElement>

    and [<AllowNullLiteral>] [<Import("Popup","leaflet")>] Popup(?options: PopupOptions, ?source: Layer) =
        inherit Layer()
        member __.options with get(): PopupOptions = jsNative and set(v: PopupOptions): unit = jsNative
        member __.getLatLng(): LatLng = jsNative
        member __.setLatLng(latlng: LatLngExpression): Popup = jsNative
        member __.getContent(): Content = jsNative
        member __.setContent(htmlContent: U2<Func<Layer, Content>, Content>): Popup = jsNative
        member __.getElement(): HTMLElement = jsNative
        member __.update(): unit = jsNative
        member __.isOpen(): bool = jsNative
        member __.bringToFront(): Popup = jsNative
        member __.bringToBack(): Popup = jsNative
        member __.openOn(map: Map): Popup = jsNative

    and [<StringEnum>] Direction =
        | Right
        | Left
        | Top
        | Bottom
        | Center
        | Auto

    and [<AllowNullLiteral>] TooltipOptions =
        inherit DivOverlayOptions
        abstract pane: string option with get, set
        abstract offset: PointExpression option with get, set
        abstract direction: Direction option with get, set
        abstract permanent: bool option with get, set
        abstract sticky: bool option with get, set
        abstract interactive: bool option with get, set
        abstract opacity: float option with get, set

    and [<AllowNullLiteral>] [<Import("Tooltip","leaflet")>] Tooltip(?options: TooltipOptions, ?source: Layer) =
        inherit Layer()
        member __.options with get(): TooltipOptions = jsNative and set(v: TooltipOptions): unit = jsNative
        member __.setOpacity(``val``: float): unit = jsNative
        member __.getLatLng(): LatLng = jsNative
        member __.setLatLng(latlng: LatLngExpression): obj = jsNative
        member __.getContent(): Content = jsNative
        member __.setContent(htmlContent: U2<Func<Layer, Content>, Content>): obj = jsNative
        member __.getElement(): HTMLElement = jsNative
        member __.update(): unit = jsNative
        member __.isOpen(): bool = jsNative
        member __.bringToFront(): obj = jsNative
        member __.bringToBack(): obj = jsNative

    and [<AllowNullLiteral>] ZoomOptions =
        abstract animate: bool option with get, set

    and [<AllowNullLiteral>] PanOptions =
        abstract animate: bool option with get, set
        abstract duration: float option with get, set
        abstract easeLinearity: float option with get, set
        abstract noMoveStart: bool option with get, set

    and [<AllowNullLiteral>] ZoomPanOptions =
        inherit ZoomOptions
        inherit PanOptions


    and [<AllowNullLiteral>] FitBoundsOptions =
        inherit ZoomOptions
        inherit PanOptions
        abstract paddingTopLeft: PointExpression option with get, set
        abstract paddingBottomRight: PointExpression option with get, set
        abstract padding: PointExpression option with get, set
        abstract maxZoom: float option with get, set

    and [<AllowNullLiteral>] LocateOptions =
        abstract watch: bool option with get, set
        abstract setView: bool option with get, set
        abstract maxZoom: float option with get, set
        abstract timeout: float option with get, set
        abstract maximumAge: float option with get, set
        abstract enableHighAccuracy: bool option with get, set

    and [<AllowNullLiteral>] [<Import("Handler","leaflet")>] Handler(map: Map) =
        inherit Class()
        member __.enable(): obj = jsNative
        member __.disable(): obj = jsNative
        member __.enabled(): bool = jsNative
        member __.addHooks(): unit = jsNative
        member __.removeHooks(): unit = jsNative

    and [<AllowNullLiteral>] Event =
        abstract ``type``: string with get, set
        abstract target: obj with get, set

    and [<AllowNullLiteral>] MouseEvent =
        inherit Event
        abstract latlng: LatLng with get, set
        abstract layerPoint: Point with get, set
        abstract containerPoint: Point with get, set
        abstract originalEvent: NativeMouseEvent with get, set

    and [<AllowNullLiteral>] KeyboardEvent =
        inherit Event
        abstract originalEvent: NativeKeyboardEvent with get, set

    and [<AllowNullLiteral>] LocationEvent =
        inherit Event
        abstract latlng: LatLng with get, set
        abstract bounds: LatLngBounds with get, set
        abstract accuracy: float with get, set
        abstract altitude: float with get, set
        abstract altitudeAccuracy: float with get, set
        abstract heading: float with get, set
        abstract speed: float with get, set
        abstract timestamp: float with get, set

    and [<AllowNullLiteral>] ErrorEvent =
        inherit Event
        abstract message: string with get, set
        abstract code: float with get, set

    and [<AllowNullLiteral>] LayerEvent =
        inherit Event
        abstract layer: Layer with get, set

    and [<AllowNullLiteral>] LayersControlEvent =
        inherit LayerEvent
        abstract name: string with get, set

    and [<AllowNullLiteral>] TileEvent =
        inherit Event
        abstract tile: HTMLImageElement with get, set
        abstract coords: Point with get, set

    and [<AllowNullLiteral>] TileErrorEvent =
        inherit TileEvent
        abstract error: Error with get, set

    and [<AllowNullLiteral>] ResizeEvent =
        inherit Event
        abstract oldSize: Point with get, set
        abstract newSize: Point with get, set

    and [<AllowNullLiteral>] GeoJSONEvent =
        inherit Event
        abstract layer: Layer with get, set
        abstract properties: obj with get, set
        abstract geometryType: string with get, set
        abstract id: string with get, set

    and [<AllowNullLiteral>] PopupEvent =
        inherit Event
        abstract popup: Popup with get, set

    and [<AllowNullLiteral>] TooltipEvent =
        inherit Event
        abstract tooltip: Tooltip with get, set

    and [<AllowNullLiteral>] DragEndEvent =
        inherit Event
        abstract distance: float with get, set

    and [<AllowNullLiteral>] ZoomAnimEvent =
        inherit Event
        abstract center: LatLng with get, set
        abstract zoom: float with get, set
        abstract noUpdate: bool with get, set

    and [<AllowNullLiteral>] DefaultMapPanes =
        abstract mapPane: HTMLElement with get, set
        abstract tilePane: HTMLElement with get, set
        abstract overlayPane: HTMLElement with get, set
        abstract shadowPane: HTMLElement with get, set
        abstract markerPane: HTMLElement with get, set
        abstract tooltipPane: HTMLElement with get, set
        abstract popupPane: HTMLElement with get, set

    and [<AllowNullLiteral>] [<Import("Map","leaflet")>] Map(element: U2<string, HTMLElement>, ?options: MapOptions) =
        inherit Evented()
        member __.boxZoom with get(): Handler = jsNative and set(v: Handler): unit = jsNative
        member __.doubleClickZoom with get(): Handler = jsNative and set(v: Handler): unit = jsNative
        member __.dragging with get(): Handler = jsNative and set(v: Handler): unit = jsNative
        member __.keyboard with get(): Handler = jsNative and set(v: Handler): unit = jsNative
        member __.scrollWheelZoom with get(): Handler = jsNative and set(v: Handler): unit = jsNative
        member __.tap with get(): Handler = jsNative and set(v: Handler): unit = jsNative
        member __.touchZoom with get(): Handler = jsNative and set(v: Handler): unit = jsNative
        member __.options with get(): MapOptions = jsNative and set(v: MapOptions): unit = jsNative
        member __.getRenderer(layer: Path): Renderer = jsNative
        member __.addControl(control: Control): obj = jsNative
        member __.removeControl(control: Control): obj = jsNative
        member __.addLayer(layer: Layer): obj = jsNative
        member __.removeLayer(layer: Layer): obj = jsNative
        member __.hasLayer(layer: Layer): bool = jsNative
        member __.eachLayer(fn: Func<Layer, unit>, ?context: obj): obj = jsNative
        member __.openPopup(popup: Popup): obj = jsNative
        member __.openPopup(content: Content, latlng: LatLngExpression, ?options: PopupOptions): obj = jsNative
        member __.closePopup(?popup: Popup): obj = jsNative
        member __.openTooltip(tooltip: Tooltip): obj = jsNative
        member __.openTooltip(content: Content, latlng: LatLngExpression, ?options: TooltipOptions): obj = jsNative
        member __.closeTooltip(?tooltip: Tooltip): obj = jsNative
        member __.setView(center: LatLngExpression, zoom: float, ?options: ZoomPanOptions): obj = jsNative
        member __.setZoom(zoom: float, ?options: ZoomPanOptions): obj = jsNative
        member __.zoomIn(?delta: float, ?options: ZoomOptions): obj = jsNative
        member __.zoomOut(?delta: float, ?options: ZoomOptions): obj = jsNative
        member __.setZoomAround(position: U2<Point, LatLngExpression>, zoom: float, ?options: ZoomOptions): obj = jsNative
        member __.fitBounds(bounds: LatLngBoundsExpression, ?options: FitBoundsOptions): obj = jsNative
        member __.fitWorld(?options: FitBoundsOptions): obj = jsNative
        member __.panTo(latlng: LatLngExpression, ?options: PanOptions): obj = jsNative
        member __.panBy(offset: PointExpression): obj = jsNative
        member __.setMaxBounds(bounds: LatLngBoundsExpression): obj = jsNative
        member __.setMinZoom(zoom: float): obj = jsNative
        member __.setMaxZoom(zoom: float): obj = jsNative
        member __.panInsideBounds(bounds: LatLngBoundsExpression, ?options: PanOptions): obj = jsNative
        member __.invalidateSize(?options: U2<bool, ZoomPanOptions>): obj = jsNative
        member __.stop(): obj = jsNative
        member __.flyTo(latlng: LatLngExpression, ?zoom: float, ?options: ZoomPanOptions): obj = jsNative
        member __.flyToBounds(bounds: LatLngBoundsExpression, ?options: FitBoundsOptions): obj = jsNative
        member __.addHandler(name: string, handlerClass: Func<unit, Handler>): obj = jsNative
        member __.remove(): obj = jsNative
        member __.createPane(name: string, ?container: HTMLElement): HTMLElement = jsNative
        member __.getPane(pane: U2<string, HTMLElement>): HTMLElement = jsNative
        member __.getPanes(): obj = jsNative
        member __.getContainer(): HTMLElement = jsNative
        member __.whenReady(fn: Func<unit, unit>, ?context: obj): obj = jsNative
        member __.getCenter(): LatLng = jsNative
        member __.getZoom(): float = jsNative
        member __.getBounds(): LatLngBounds = jsNative
        member __.getMinZoom(): float = jsNative
        member __.getMaxZoom(): float = jsNative
        member __.getBoundsZoom(bounds: LatLngBoundsExpression, ?inside: bool): float = jsNative
        member __.getSize(): Point = jsNative
        member __.getPixelBounds(): Bounds = jsNative
        member __.getPixelOrigin(): Point = jsNative
        member __.getPixelWorldBounds(?zoom: float): Bounds = jsNative
        member __.getZoomScale(toZoom: float, fromZoom: float): float = jsNative
        member __.getScaleZoom(scale: float, fromZoom: float): float = jsNative
        member __.project(latlng: LatLngExpression, zoom: float): Point = jsNative
        member __.unproject(point: PointExpression, zoom: float): LatLng = jsNative
        member __.layerPointToLatLng(point: PointExpression): LatLng = jsNative
        member __.latLngToLayerPoint(latlng: LatLngExpression): Point = jsNative
        member __.wrapLatLng(latlng: LatLngExpression): LatLng = jsNative
        member __.distance(latlng1: LatLngExpression, latlng2: LatLngExpression): float = jsNative
        member __.containerPointToLayerPoint(point: PointExpression): Point = jsNative
        member __.layerPointToContainerPoint(point: PointExpression): Point = jsNative
        member __.latLngToContainerPoint(latlng: LatLngExpression): Point = jsNative
        member __.mouseEventToContainerPoint(ev: MouseEvent): Point = jsNative
        member __.mouseEventToLayerPoint(ev: MouseEvent): Point = jsNative
        member __.mouseEventToLatLng(ev: MouseEvent): LatLng = jsNative
        member __.locate(?options: LocateOptions): obj = jsNative
        member __.stopLocate(): obj = jsNative

    and [<AllowNullLiteral>] BaseIconOptions =
        inherit LayerOptions
        abstract iconUrl: string option with get, set
        abstract iconRetinaUrl: string option with get, set
        abstract iconSize: PointExpression option with get, set
        abstract iconAnchor: PointExpression option with get, set
        abstract popupAnchor: PointExpression option with get, set
        abstract shadowUrl: string option with get, set
        abstract shadowRetinaUrl: string option with get, set
        abstract shadowSize: PointExpression option with get, set
        abstract shadowAnchor: PointExpression option with get, set
        abstract className: string option with get, set

    and [<AllowNullLiteral>] IconOptions =
        inherit BaseIconOptions
        abstract iconUrl: string with get, set

    and [<AllowNullLiteral>] [<Import("BaseIcon","leaflet")>] BaseIcon() =
        inherit Layer()
        member __.options with get(): BaseIconOptions = jsNative and set(v: BaseIconOptions): unit = jsNative
        member __.createIcon(?oldIcon: HTMLElement): HTMLElement = jsNative
        member __.createShadow(?oldIcon: HTMLElement): HTMLElement = jsNative

    and [<AllowNullLiteral>] [<Import("Icon","leaflet")>] Icon(options: IconOptions) =
        inherit BaseIcon()
        member __.options with get(): IconOptions = jsNative and set(v: IconOptions): unit = jsNative

    and [<AllowNullLiteral>] DivIconOptions =
        inherit BaseIconOptions
        abstract html: string option with get, set
        abstract bgPos: PointExpression option with get, set
        abstract iconSize: PointExpression option with get, set
        abstract iconAnchor: PointExpression option with get, set
        abstract popupAnchor: PointExpression option with get, set
        abstract className: string option with get, set

    and [<AllowNullLiteral>] [<Import("DivIcon","leaflet")>] DivIcon(?options: DivIconOptions) =
        inherit BaseIcon()
        member __.options with get(): DivIconOptions = jsNative and set(v: DivIconOptions): unit = jsNative

    and [<AllowNullLiteral>] MarkerOptions =
        inherit InteractiveLayerOptions
        abstract icon: U2<Icon, DivIcon> option with get, set
        abstract clickable: bool option with get, set
        abstract draggable: bool option with get, set
        abstract keyboard: bool option with get, set
        abstract title: string option with get, set
        abstract alt: string option with get, set
        abstract zIndexOffset: float option with get, set
        abstract opacity: float option with get, set
        abstract riseOnHover: bool option with get, set
        abstract riseOffset: float option with get, set

    and [<AllowNullLiteral>] [<Import("Marker","leaflet")>] Marker(latlng: LatLngExpression, ?options: MarkerOptions) =
        inherit Layer()
        member __.options with get(): MarkerOptions = jsNative and set(v: MarkerOptions): unit = jsNative
        member __.dragging with get(): Handler = jsNative and set(v: Handler): unit = jsNative
        member __.getLatLng(): LatLng = jsNative
        member __.setLatLng(latlng: LatLngExpression): obj = jsNative
        member __.setZIndexOffset(offset: float): obj = jsNative
        member __.setIcon(icon: U2<Icon, DivIcon>): obj = jsNative
        member __.setOpacity(opacity: float): obj = jsNative
        member __.getElement(): HTMLElement = jsNative

    type [<Import("*","leaflet")>] Globals =
        static member latLng(latitude: float, longitude: float, ?altitude: float): LatLng = jsNative
        static member latLng(coords: U4<LatLngTuple, float * float * float, LatLngLiteral, obj>): LatLng = jsNative
        static member latLngBounds(southWest: LatLngExpression, northEast: LatLngExpression): LatLngBounds = jsNative
        static member latLngBounds(latlngs: LatLngBoundsLiteral): LatLngBounds = jsNative
        static member point(x: float, y: float, ?round: bool): Point = jsNative
        static member point(coords: U2<PointTuple, obj>): Point = jsNative
        static member bounds(topLeft: PointExpression, bottomRight: PointExpression): Bounds = jsNative
        static member bounds(points: U2<ResizeArray<Point>, BoundsLiteral>): Bounds = jsNative
        static member gridLayer(?options: GridLayerOptions): GridLayer = jsNative
        static member tileLayer(urlTemplate: string, ?options: TileLayerOptions): TileLayer = jsNative
        static member imageOverlay(imageUrl: string, bounds: LatLngBoundsExpression, ?options: ImageOverlayOptions): ImageOverlay = jsNative
        static member polyline(latlngs: ResizeArray<LatLngExpression>, ?options: PolylineOptions): Polyline = jsNative
        static member polygon(latlngs: ResizeArray<LatLngExpression>, ?options: PolylineOptions): Polygon = jsNative
        static member rectangle(latLngBounds: LatLngBoundsExpression, ?options: PolylineOptions): Rectangle = jsNative
        static member circleMarker(latlng: LatLngExpression, ?options: CircleMarkerOptions): CircleMarker = jsNative
        static member circle(latlng: LatLngExpression, ?options: CircleMarkerOptions): Circle = jsNative
        static member circle(latlng: LatLngExpression, radius: float, ?options: CircleMarkerOptions): Circle = jsNative
        static member svg(?options: RendererOptions): SVG = jsNative
        static member canvas(?options: RendererOptions): Canvas = jsNative
        static member layerGroup(layers: ResizeArray<Layer>): LayerGroup = jsNative
        static member featureGroup(?layers: ResizeArray<Layer>): FeatureGroup = jsNative
        static member geoJSON(?geojson: GeoJSON.GeoJsonObject, ?options: GeoJSONOptions): GeoJSON = jsNative
        static member popup(?options: PopupOptions, ?source: Layer): Popup = jsNative
        static member tooltip(?options: TooltipOptions, ?source: Layer): Tooltip = jsNative
        static member map(element: U2<string, HTMLElement>, ?options: MapOptions): Map = jsNative
        static member icon(options: IconOptions): Icon = jsNative
        static member divIcon(?options: DivIconOptions): DivIcon = jsNative
        static member marker(latlng: LatLngExpression, ?options: MarkerOptions): Marker = jsNative

    module LineUtil =
        type [<Import("LineUtil","leaflet")>] Globals =
            static member simplify(points: ResizeArray<PointExpression>, tolerance: float): ResizeArray<Point> = jsNative
            static member pointToSegmentDistance(p: PointExpression, p1: PointExpression, p2: PointExpression): float = jsNative
            static member closestPointOnSegment(p: PointExpression, p1: PointExpression, p2: PointExpression): Point = jsNative



    module PolyUtil =
        type [<Import("PolyUtil","leaflet")>] Globals =
            static member clipPolygon(points: ResizeArray<PointExpression>, bounds: BoundsExpression, ?round: bool): ResizeArray<Point> = jsNative



    module CRS =
        type [<Import("CRS","leaflet")>] Globals =
            static member EPSG3395 with get(): CRS = jsNative and set(v: CRS): unit = jsNative
            static member EPSG3857 with get(): CRS = jsNative and set(v: CRS): unit = jsNative
            static member EPSG4326 with get(): CRS = jsNative and set(v: CRS): unit = jsNative
            static member Earth with get(): CRS = jsNative and set(v: CRS): unit = jsNative
            static member Simple with get(): CRS = jsNative and set(v: CRS): unit = jsNative



    module Projection =
        type [<Import("Projection","leaflet")>] Globals =
            static member LonLat with get(): Projection = jsNative and set(v: Projection): unit = jsNative
            static member Mercator with get(): Projection = jsNative and set(v: Projection): unit = jsNative
            static member SphericalMercator with get(): Projection = jsNative and set(v: Projection): unit = jsNative



    module TileLayer =
        type [<AllowNullLiteral>] [<Import("TileLayer.WMS","leaflet")>] WMS(baseUrl: string, options: WMSOptions) =
            inherit TileLayer(baseUrl)
            member __.wmsParams with get(): WMSParams = jsNative and set(v: WMSParams): unit = jsNative
            member __.options with get(): WMSOptions = jsNative and set(v: WMSOptions): unit = jsNative
            member __.setParams(``params``: WMSParams, ?noRedraw: bool): obj = jsNative
            



    module tileLayer =
        type [<Import("tileLayer","leaflet")>] Globals =
            static member wms(baseUrl: string, ?options: WMSOptions): TileLayer.WMS = jsNative



    module SVG =
        type [<Import("SVG","leaflet")>] Globals =
            static member create(name: string): SVGElement = jsNative
            static member pointsToPath(rings: ResizeArray<PointExpression>, close: bool): string = jsNative



    module Control =
        type [<AllowNullLiteral>] ZoomOptions =
            inherit ControlOptions
            abstract zoomInText: string option with get, set
            abstract zoomInTitle: string option with get, set
            abstract zoomOutText: string option with get, set
            abstract zoomOutTitle: string option with get, set

        and [<AllowNullLiteral>] [<Import("Control.Zoom","leaflet")>] Zoom(?options: ZoomOptions) =
            inherit Control()
            member __.options with get(): ZoomOptions = jsNative and set(v: ZoomOptions): unit = jsNative

        and [<AllowNullLiteral>] AttributionOptions =
            inherit ControlOptions
            abstract prefix: U2<string, bool> option with get, set

        and [<AllowNullLiteral>] [<Import("Control.Attribution","leaflet")>] Attribution(?options: AttributionOptions) =
            inherit Control()
            member __.options with get(): AttributionOptions = jsNative and set(v: AttributionOptions): unit = jsNative
            member __.setPrefix(prefix: string): obj = jsNative
            member __.addAttribution(text: string): obj = jsNative
            member __.removeAttribution(text: string): obj = jsNative

        and [<AllowNullLiteral>] LayersOptions =
            inherit ControlOptions
            abstract collapsed: bool option with get, set
            abstract autoZIndex: bool option with get, set
            abstract hideSingleBase: bool option with get, set

        and [<AllowNullLiteral>] LayersObject =
            [<Emit("$0[$1]{{=$2}}")>] abstract Item: name: string -> Layer with get, set

        and [<AllowNullLiteral>] [<Import("Control.Layers","leaflet")>] Layers(?baseLayers: LayersObject, ?overlays: LayersObject, ?options: LayersOptions) =
            inherit Control()
            member __.options with get(): LayersOptions = jsNative and set(v: LayersOptions): unit = jsNative
            member __.addBaseLayer(layer: Layer, name: string): obj = jsNative
            member __.addOverlay(layer: Layer, name: string): obj = jsNative
            member __.removeLayer(layer: Layer): obj = jsNative
            member __.expand(): obj = jsNative
            member __.collapse(): obj = jsNative

        and [<AllowNullLiteral>] ScaleOptions =
            inherit ControlOptions
            abstract maxWidth: float option with get, set
            abstract metric: bool option with get, set
            abstract imperial: bool option with get, set
            abstract updateWhenIdle: bool option with get, set

        and [<AllowNullLiteral>] [<Import("Control.Scale","leaflet")>] Scale(?options: ScaleOptions) =
            inherit Control()
            member __.options with get(): ScaleOptions = jsNative and set(v: ScaleOptions): unit = jsNative



    module control =
        type [<Import("control","leaflet")>] Globals =
            static member zoom(?options: Control.ZoomOptions): Control.Zoom = jsNative
            static member attribution(?options: Control.AttributionOptions): Control.Attribution = jsNative
            static member layers(?baseLayers: Control.LayersObject, ?overlays: Control.LayersObject, ?options: Control.LayersOptions): Control.Layers = jsNative
            static member scale(?options: Control.ScaleOptions): Control.Scale = jsNative



    module DomEvent =
        type [<Import("DomEvent","leaflet")>] Globals =
            static member on(el: HTMLElement, types: string, fn: EventHandlerFn, ?context: obj): obj = jsNative
            static member on(el: HTMLElement, eventMap: obj, ?context: obj): obj = jsNative
            static member off(el: HTMLElement, types: string, fn: EventHandlerFn, ?context: obj): obj = jsNative
            static member off(el: HTMLElement, eventMap: obj, ?context: obj): obj = jsNative
            static member stopPropagation(ev: Event): obj = jsNative
            static member disableScrollPropagation(el: HTMLElement): obj = jsNative
            static member disableClickPropagation(el: HTMLElement): obj = jsNative
            static member preventDefault(ev: Event): obj = jsNative
            static member stop(ev: Event): obj = jsNative
            static member getMousePosition(ev: Event, ?container: HTMLElement): Point = jsNative
            static member getWheelDelta(ev: Event): float = jsNative
            static member addListener(el: HTMLElement, types: string, fn: EventHandlerFn, ?context: obj): obj = jsNative
            static member addListener(el: HTMLElement, eventMap: obj, ?context: obj): obj = jsNative
            static member removeListener(el: HTMLElement, types: string, fn: EventHandlerFn, ?context: obj): obj = jsNative
            static member removeListener(el: HTMLElement, eventMap: obj, ?context: obj): obj = jsNative



    module Icon =
        type [<AllowNullLiteral>] DefaultIconOptions =
            inherit BaseIconOptions
            abstract imagePath: string option with get, set

        and [<AllowNullLiteral>] [<Import("Icon.Default","leaflet")>] Default(?options: DefaultIconOptions) =
            inherit BaseIcon()
            member __.imagePath with get(): string option = jsNative and set(v: string option): unit = jsNative
            member __.options with get(): DefaultIconOptions = jsNative and set(v: DefaultIconOptions): unit = jsNative



    module Browser =
        type [<Import("Browser","leaflet")>] Globals =
            static member ie with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member ielt9 with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member edge with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member webkit with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member gecko with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member android with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member android23 with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member chrome with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member safari with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member win with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member ie3d with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member webkit3d with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member gecko3d with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member opera12 with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member any3d with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member mobile with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member mobileWebkit with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member mobiWebkit3d with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member mobileOpera with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member mobileGecko with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member touch with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member msPointer with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member pointer with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member retina with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member canvas with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member vml with get(): bool = jsNative and set(v: bool): unit = jsNative
            static member svg with get(): bool = jsNative and set(v: bool): unit = jsNative



    module Util =
        type [<Import("Util","leaflet")>] Globals =
            static member lastId with get(): string = jsNative and set(v: string): unit = jsNative
            static member emptyImageUrl with get(): string = jsNative and set(v: string): unit = jsNative
            static member extend(dest: obj, ?src: obj): obj = jsNative
            static member create(proto: obj, ?properties: obj): obj = jsNative
            static member bind(fn: Func<unit, unit>, [<ParamArray>] obj: obj[]): Func<unit, unit> = jsNative
            static member stamp(obj: obj): float = jsNative
            static member throttle(fn: Func<unit, unit>, time: float, context: obj): Func<unit, unit> = jsNative
            static member wrapNum(num: float, range: ResizeArray<float>, ?includeMax: bool): float = jsNative
            static member falseFn(): Func<unit, obj> = jsNative
            static member formatNum(num: float, ?digits: float): float = jsNative
            static member trim(str: string): string = jsNative
            static member splitWords(str: string): ResizeArray<string> = jsNative
            static member setOptions(obj: obj, options: obj): obj = jsNative
            static member getParamString(obj: obj, ?existingUrl: string, ?uppercase: bool): string = jsNative
            static member template(str: string, data: obj): string = jsNative
            static member isArray(obj: obj): bool = jsNative
            static member indexOf(array: ResizeArray<obj>, el: obj): float = jsNative
            static member requestAnimFrame(fn: Func<unit, unit>, ?context: obj, ?immediate: bool): float = jsNative
            static member cancelAnimFrame(id: float): unit = jsNative