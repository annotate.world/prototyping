namespace Fable.Import
open System
open System.Text.RegularExpressions
open Fable.Core
open Fable.Import.JS
open Fable.Import.Browser


type NativeMouseEvent = MouseEvent

module Paper =
    type [<AllowNullLiteral>] settingsType =
        abstract applyMatrix: bool with get, set
        abstract handleSize: float with get, set
        abstract hitTolerance: float with get, set

    and [<AllowNullLiteral>] [<Import("Matrix","paper")>] Matrix(a: float, c: float, b: float, d: float, tx: float, ty: float) =
        member __.a with get(): float = jsNative and set(v: float): unit = jsNative
        member __.c with get(): float = jsNative and set(v: float): unit = jsNative
        member __.b with get(): float = jsNative and set(v: float): unit = jsNative
        member __.d with get(): float = jsNative and set(v: float): unit = jsNative
        member __.tx with get(): float = jsNative and set(v: float): unit = jsNative
        member __.ty with get(): float = jsNative and set(v: float): unit = jsNative
        member __.values with get(): float = jsNative and set(v: float): unit = jsNative
        member __.translation with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.scaling with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.rotation with get(): float = jsNative and set(v: float): unit = jsNative
        member __.set(a: float, c: float, b: float, d: float, tx: float, ty: float): Matrix = jsNative
        member __.clone(): Matrix = jsNative
        member __.equals(matrix: Matrix): bool = jsNative
        member __.toString(): string = jsNative
        member __.reset(): unit = jsNative
        member __.apply(): bool = jsNative
        member __.translate(point: Point): Matrix = jsNative
        member __.translate(dx: float, dy: float): Matrix = jsNative
        member __.scale(scale: float, ?center: Point): Matrix = jsNative
        member __.scale(hor: float, ver: float, ?center: Point): Matrix = jsNative
        member __.rotate(angle: float, center: Point): Matrix = jsNative
        member __.rotate(angle: float, x: float, y: float): Matrix = jsNative
        member __.shear(shear: Point, ?center: Point): Matrix = jsNative
        member __.shear(hor: float, ver: float, ?center: Point): Matrix = jsNative
        member __.skew(skew: Point, ?center: Point): Matrix = jsNative
        member __.skew(hor: float, ver: float, ?center: Point): Matrix = jsNative
        member __.concatenate(mx: Matrix): Matrix = jsNative
        member __.preConcatenate(mx: Matrix): Matrix = jsNative
        member __.chain(mx: Matrix): Matrix = jsNative
        member __.isIdentity(): bool = jsNative
        member __.isInvertible(): bool = jsNative
        member __.isSingular(): bool = jsNative
        member __.transform(point: Point): Matrix = jsNative
        member __.transform(src: ResizeArray<float>, dst: ResizeArray<float>, count: float): ResizeArray<float> = jsNative
        member __.inverseTransform(point: Point): Matrix = jsNative
        member __.decompose(): obj = jsNative
        member __.inverted(): Matrix = jsNative
        member __.applyToContext(ctx: CanvasRenderingContext2D): unit = jsNative

    and [<AllowNullLiteral>] [<Import("Point","paper")>] Point(x: float, y: float) =                
        member __.x with get(): float = jsNative and set(v: float): unit = jsNative
        member __.y with get(): float = jsNative and set(v: float): unit = jsNative
        member __.length with get(): float = jsNative and set(v: float): unit = jsNative
        member __.angle with get(): float = jsNative and set(v: float): unit = jsNative
        member __.angleInRadians with get(): float = jsNative and set(v: float): unit = jsNative
        member __.quadrant with get(): float = jsNative and set(v: float): unit = jsNative
        member __.selected with get(): bool = jsNative and set(v: bool): unit = jsNative
        static member min(point1: Point, point2: Point): Point = jsNative
        static member max(point1: Point, point2: Point): Point = jsNative
        static member random(): Point = jsNative
        member __.equals(point: Point): bool = jsNative
        member __.clone(): Point = jsNative
        member __.toString(): string = jsNative
        member __.getAngle(point: Point): float = jsNative
        member __.getAngleInRadians(point: Point): float = jsNative
        member __.getDirectedAngle(point: Point): float = jsNative
        member __.getDistance(point: Point, ?squared: bool): float = jsNative
        member __.normalize(?length: float): Point = jsNative
        member __.rotate(angle: float, ?center: Point): Point = jsNative
        member __.transform(matrix: Matrix): Point = jsNative
        member __.isInside(rect: Rectangle): bool = jsNative
        member __.isClose(point: Point, tolerance: float): bool = jsNative
        member __.isColinear(point: Point): bool = jsNative
        member __.isOrthogonal(point: Point): bool = jsNative
        member __.isZero(): bool = jsNative
        member __.isNan(): bool = jsNative
        member __.dot(point: Point): float = jsNative
        member __.cross(point: Point): float = jsNative
        member __.project(point: Point): Point = jsNative
        member __.round(): Point = jsNative
        member __.ceil(): Point = jsNative
        member __.floor(): Point = jsNative
        member __.abs(): Point = jsNative
        member __.add(point: Point): Point = jsNative
        member __.add(point: ResizeArray<float>): Point = jsNative
        member __.subtract(point: Point): Point = jsNative
        member __.subtract(point: ResizeArray<float>): Point = jsNative
        member __.multiply(point: Point): Point = jsNative
        member __.multiply(point: ResizeArray<float>): Point = jsNative
        member __.multiply(point: float): Point = jsNative
        member __.divide(point: Point): Point = jsNative
        member __.divide(point: ResizeArray<float>): Point = jsNative
        member __.divide(point: float): Point = jsNative

    and [<AllowNullLiteral>] [<Import("Rectangle","paper")>] Rectangle(rt: Rectangle) =
        member __.x with get(): float = jsNative and set(v: float): unit = jsNative
        member __.y with get(): float = jsNative and set(v: float): unit = jsNative
        member __.width with get(): float = jsNative and set(v: float): unit = jsNative
        member __.height with get(): float = jsNative and set(v: float): unit = jsNative
        member __.point with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.size with get(): Size = jsNative and set(v: Size): unit = jsNative
        member __.left with get(): float = jsNative and set(v: float): unit = jsNative
        member __.top with get(): float = jsNative and set(v: float): unit = jsNative
        member __.right with get(): float = jsNative and set(v: float): unit = jsNative
        member __.bottom with get(): float = jsNative and set(v: float): unit = jsNative
        member __.center with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.topLeft with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.topRight with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.bottomLeft with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.bottomRight with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.leftCenter with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.topCenter with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.rightCenter with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.bottomCenter with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.area with get(): float = jsNative and set(v: float): unit = jsNative
        member __.selected with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.clone(): Rectangle = jsNative
        member __.equals(rect: Rectangle): bool = jsNative
        member __.toString(): string = jsNative
        member __.isEmpty(): bool = jsNative
        member __.contains(point: Point): bool = jsNative
        member __.contains(rect: Rectangle): bool = jsNative
        member __.intersects(rect: Rectangle): bool = jsNative
        member __.intersect(rect: Rectangle): Rectangle = jsNative
        member __.unite(rect: Rectangle): Rectangle = jsNative
        member __.``include``(point: Point): Rectangle = jsNative
        member __.expand(amount: U3<float, Size, Point>): Rectangle = jsNative
        member __.expand(hor: float, ver: float): Rectangle = jsNative
        member __.scale(amount: float): Rectangle = jsNative
        member __.scale(hor: float, ver: float): Rectangle = jsNative

    and [<AllowNullLiteral>] [<Import("Size","paper")>] Size(point: Point) =
        member __.width with get(): float = jsNative and set(v: float): unit = jsNative
        member __.height with get(): float = jsNative and set(v: float): unit = jsNative
        static member min(size1: Size, size2: Size): Size = jsNative
        static member max(size1: Size, size2: Size): Size = jsNative
        static member random(): Size = jsNative
        member __.equals(): bool = jsNative
        member __.clone(): Size = jsNative
        member __.toString(): string = jsNative
        member __.isZero(): bool = jsNative
        member __.isNan(): bool = jsNative
        member __.round(): Size = jsNative
        member __.ceil(): Size = jsNative
        member __.floor(): Size = jsNative
        member __.abs(): Size = jsNative
        member __.multiply(point: Size): Size = jsNative
        member __.multiply(point: ResizeArray<float>): Size = jsNative
        member __.multiply(point: float): Size = jsNative
        member __.divide(point: Size): Size = jsNative
        member __.divide(point: ResizeArray<float>): Size = jsNative
        member __.divide(point: float): Size = jsNative

    and [<AllowNullLiteral>] IFrameEvent =
        abstract count: float with get, set
        abstract time: float with get, set
        abstract delta: float with get, set

    and [<AllowNullLiteral>] [<Import("PaperScope","paper")>] PaperScope() =
        member __.version with get(): string = jsNative and set(v: string): unit = jsNative
        member __.settings with get(): obj = jsNative and set(v: obj): unit = jsNative
        member __.project with get(): Project = jsNative and set(v: Project): unit = jsNative
        member __.projects with get(): ResizeArray<Project> = jsNative and set(v: ResizeArray<Project>): unit = jsNative
        member __.view with get(): View = jsNative and set(v: View): unit = jsNative
        member __.tool with get(): Tool = jsNative and set(v: Tool): unit = jsNative
        member __.tools with get(): ResizeArray<Tool> = jsNative and set(v: ResizeArray<Tool>): unit = jsNative
        member __.install(scope: obj): unit = jsNative
        member __.setup(canvas: U2<HTMLCanvasElement, string>): unit = jsNative
        member __.activate(): unit = jsNative
        static member get(id: string): PaperScope = jsNative

    and [<AllowNullLiteral>] [<Import("Item","paper")>] Item() =
        member __.tangent with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.normal with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.curvature with get(): float = jsNative and set(v: float): unit = jsNative
        member __.id with get(): float = jsNative and set(v: float): unit = jsNative
        member __.className with get(): string = jsNative and set(v: string): unit = jsNative
        member __.name with get(): string = jsNative and set(v: string): unit = jsNative
        member __.style with get(): Style = jsNative and set(v: Style): unit = jsNative
        member __.visible with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.blendMode with get(): string = jsNative and set(v: string): unit = jsNative
        member __.opacity with get(): float = jsNative and set(v: float): unit = jsNative
        member __.selected with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.clipMask with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.data with get(): obj = jsNative and set(v: obj): unit = jsNative
        member __.position with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.pivot with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.bounds with get(): Rectangle = jsNative and set(v: Rectangle): unit = jsNative
        member __.strokeBounds with get(): Rectangle = jsNative and set(v: Rectangle): unit = jsNative
        member __.handleBounds with get(): Rectangle = jsNative and set(v: Rectangle): unit = jsNative
        member __.rotation with get(): float = jsNative and set(v: float): unit = jsNative
        member __.scaling with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.matrix with get(): Matrix = jsNative and set(v: Matrix): unit = jsNative
        member __.globalMatrix with get(): Matrix = jsNative and set(v: Matrix): unit = jsNative
        member __.applyMatrix with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.project with get(): Project = jsNative and set(v: Project): unit = jsNative
        member __.view with get(): View = jsNative and set(v: View): unit = jsNative
        member __.layer with get(): Layer = jsNative and set(v: Layer): unit = jsNative
        member __.parent with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.children with get(): ResizeArray<Item> = jsNative and set(v: ResizeArray<Item>): unit = jsNative
        member __.firstChild with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.lastChild with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.nextSibling with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.previousSibling with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.index with get(): float = jsNative and set(v: float): unit = jsNative
        member __.strokeColor with get(): U2<Color, string> = jsNative and set(v: U2<Color, string>): unit = jsNative
        member __.strokeWidth with get(): float = jsNative and set(v: float): unit = jsNative
        member __.strokeCap with get(): string = jsNative and set(v: string): unit = jsNative
        member __.strokeJoin with get(): string = jsNative and set(v: string): unit = jsNative
        member __.dashOffset with get(): float = jsNative and set(v: float): unit = jsNative
        member __.strokeScaling with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.dashArray with get(): ResizeArray<float> = jsNative and set(v: ResizeArray<float>): unit = jsNative
        member __.miterLimit with get(): float = jsNative and set(v: float): unit = jsNative
        member __.windingRule with get(): string = jsNative and set(v: string): unit = jsNative
        member __.fillColor with get(): U2<Color, string> = jsNative and set(v: U2<Color, string>): unit = jsNative
        member __.fillRule with get(): string = jsNative and set(v: string): unit = jsNative
        member __.selectedColor with get(): U2<Color, string> = jsNative and set(v: U2<Color, string>): unit = jsNative
        member __.onFrame with get(): Func<IFrameEvent, unit> = jsNative and set(v: Func<IFrameEvent, unit>): unit = jsNative
        member __.onMouseDown with get(): Func<MouseEvent, unit> = jsNative and set(v: Func<MouseEvent, unit>): unit = jsNative
        member __.onMouseUp with get(): Func<MouseEvent, unit> = jsNative and set(v: Func<MouseEvent, unit>): unit = jsNative
        member __.onClick with get(): Func<MouseEvent, unit> = jsNative and set(v: Func<MouseEvent, unit>): unit = jsNative
        member __.onDoubleClick with get(): Func<MouseEvent, unit> = jsNative and set(v: Func<MouseEvent, unit>): unit = jsNative
        member __.onMouseMove with get(): Func<MouseEvent, unit> = jsNative and set(v: Func<MouseEvent, unit>): unit = jsNative
        member __.onMouseEnter with get(): Func<MouseEvent, unit> = jsNative and set(v: Func<MouseEvent, unit>): unit = jsNative
        member __.onMouseLeave with get(): Func<MouseEvent, unit> = jsNative and set(v: Func<MouseEvent, unit>): unit = jsNative
        member __.set(props: obj): Item = jsNative
        member __.clone(?insert: bool): Item = jsNative
        member __.copyTo(item: Item): Item = jsNative
        member __.rasterize(resolution: float): Raster = jsNative
        member __.contains(point: Point): bool = jsNative
        member __.isInside(rect: Rectangle): bool = jsNative
        member __.intersects(item: Item): bool = jsNative
        member __.hitTest(point: Point, ?options: obj): HitResult = jsNative
        member __.matches(``match``: obj): bool = jsNative
        member __.matches(name: string, compare: obj): bool = jsNative
        member __.getItems(``match``: obj): ResizeArray<Item> = jsNative
        member __.getItem(``match``: obj): Item = jsNative
        member __.exportJSON(?options: obj): string = jsNative
        member __.importJSON(json: string): unit = jsNative
        member __.exportSVG(?options: obj): SVGElement = jsNative
        member __.importSVG(svg: U2<SVGElement, string>, ?options: obj): Item = jsNative
        member __.addChild(item: Item): Item = jsNative
        member __.insertChild(index: float, item: Item): Item = jsNative
        member __.addChildren(items: ResizeArray<Item>): ResizeArray<Item> = jsNative
        member __.insertChildren(index: float, items: ResizeArray<Item>): ResizeArray<Item> = jsNative
        member __.insertAbove(item: Item): Item = jsNative
        member __.insertBelow(item: Item): Item = jsNative
        member __.moveAbove(item: Item): bool = jsNative
        member __.moveBelow(item: Item): bool = jsNative
        member __.sendToBack(): unit = jsNative
        member __.bringToFront(): unit = jsNative
        member __.reduce(): Item = jsNative
        member __.remove(): bool = jsNative
        member __.replaceWith(item: Item): bool = jsNative
        member __.removeChildren(): ResizeArray<Item> = jsNative
        member __.removeChildren(from: float, ?``to``: float): ResizeArray<Item> = jsNative
        member __.reverseChildren(): unit = jsNative
        member __.isEmpty(): bool = jsNative
        member __.hasFill(): bool = jsNative
        member __.hasStroke(): bool = jsNative
        member __.hasShadow(): bool = jsNative
        member __.hasChildren(): bool = jsNative
        member __.isInserted(): bool = jsNative
        member __.isAbove(item: Item): bool = jsNative
        member __.isBelow(item: Item): bool = jsNative
        member __.isParent(item: Item): bool = jsNative
        member __.isChild(item: Item): bool = jsNative
        member __.isDescendant(item: Item): bool = jsNative
        member __.isAncestor(item: Item): bool = jsNative
        member __.isGroupedWith(item: Item): bool = jsNative
        member __.translate(delta: Point): Point = jsNative
        member __.rotate(angle: float, ?center: Point): unit = jsNative
        member __.getRotation(): float = jsNative
        member __.scale(scale: float, ?center: Point): unit = jsNative
        member __.scale(hor: float, ver: float, ?center: Point): unit = jsNative
        member __.shear(shear: float, ?center: Point): unit = jsNative
        member __.shear(hor: float, ver: float, ?center: Point): unit = jsNative
        member __.skew(skew: Point, ?center: Point): unit = jsNative
        member __.skew(hor: float, ver: float, ?center: Point): unit = jsNative
        member __.transform(matrix: Matrix): unit = jsNative
        member __.globalToLocal(point: Point): Point = jsNative
        member __.localToGlobal(point: Point): Point = jsNative
        member __.parentToLocal(point: Point): Point = jsNative
        member __.localToParent(point: Point): Point = jsNative
        member __.fitBounds(rectangle: Rectangle, ?fill: bool): unit = jsNative
        member __.on(``type``: string, callback: Func<ToolEvent, unit>): Tool = jsNative
        member __.on(param: obj): Tool = jsNative
        member __.off(``type``: string, callback: Func<ToolEvent, unit>): Tool = jsNative
        member __.off(param: obj): Tool = jsNative
        member __.emit(``type``: string, ``event``: obj): bool = jsNative
        member __.responds(``type``: string): bool = jsNative
        member __.on(``type``: string, callback: Func<unit, unit>): Item = jsNative
        member __.removeOn(``object``: obj): unit = jsNative
        member __.removeOnMove(): unit = jsNative
        member __.removeOnDown(): unit = jsNative
        member __.removeOnDrag(): unit = jsNative
        member __.removeOnUp(): unit = jsNative

    and [<AllowNullLiteral>] [<Import("Group","paper")>] Group(?``object``: obj) =
        inherit Item()
        member __.clipped with get(): bool = jsNative and set(v: bool): unit = jsNative

    and [<AllowNullLiteral>] [<Import("Layer","paper")>] Layer(?``object``: obj) =
        inherit Group()
        member __.activate(): unit = jsNative

    and [<AllowNullLiteral>] [<Import("Shape","paper")>] Shape() =
        inherit Item()
        member __.``type`` with get(): string = jsNative and set(v: string): unit = jsNative
        member __.size with get(): Size = jsNative and set(v: Size): unit = jsNative
        member __.radius with get(): U2<float, Size> = jsNative and set(v: U2<float, Size>): unit = jsNative
        static member Circle(center: Point, radius: float): Shape = jsNative
        static member Circle(``object``: obj): Shape = jsNative
        static member Rectangle(rectangle: Rectangle, ?radius: float): Shape = jsNative
        static member Rectangle(point: Point, size: Size): Shape = jsNative
        static member Rectangle(from: Point, ``to``: Point): Shape = jsNative
        static member Rectangle(``object``: obj): Shape = jsNative
        static member Ellipse(rectangle: Rectangle): Shape = jsNative
        static member Ellipse(``object``: obj): Shape = jsNative

    and [<AllowNullLiteral>] [<Import("Raster","paper")>] Raster(config: obj) =
        inherit Item()
        member __.size with get(): Size = jsNative and set(v: Size): unit = jsNative
        member __.width with get(): float = jsNative and set(v: float): unit = jsNative
        member __.height with get(): float = jsNative and set(v: float): unit = jsNative
        member __.resolution with get(): Size = jsNative and set(v: Size): unit = jsNative
        member __.image with get(): U2<HTMLImageElement, HTMLCanvasElement> = jsNative and set(v: U2<HTMLImageElement, HTMLCanvasElement>): unit = jsNative
        member __.canvas with get(): HTMLCanvasElement = jsNative and set(v: HTMLCanvasElement): unit = jsNative
        member __.context with get(): CanvasRenderingContext2D = jsNative and set(v: CanvasRenderingContext2D): unit = jsNative
        member __.source with get(): U3<HTMLImageElement, HTMLCanvasElement, string> = jsNative and set(v: U3<HTMLImageElement, HTMLCanvasElement, string>): unit = jsNative
        member __.getSubCanvas(rect: Rectangle): HTMLCanvasElement = jsNative
        member __.getSubRaster(rect: Rectangle): Raster = jsNative
        member __.toDataURL(): string = jsNative
        member __.drawImage(image: U2<HTMLImageElement, HTMLCanvasElement>, point: Point): unit = jsNative
        member __.getAverageColor(``object``: U3<Path, Rectangle, Point>): Color = jsNative
        member __.getPixel(x: float, y: float): Color = jsNative
        member __.getPixel(point: Point): Color = jsNative
        member __.setPixel(x: float, y: float, color: Color): unit = jsNative
        member __.setPixel(point: Point, color: Color): unit = jsNative
        member __.createImageData(size: Size): ImageData = jsNative
        member __.getImageData(rect: Rectangle): ImageData = jsNative
        member __.getImageData(data: ImageData, point: Point): unit = jsNative

    and [<AllowNullLiteral>] [<Import("PlacedSymbol","paper")>] PlacedSymbol(symbol: Symbol, ?point: Point) =
        inherit Item()
        member __.symbol with get(): Symbol = jsNative and set(v: Symbol): unit = jsNative

    and [<AllowNullLiteral>] [<Import("HitResult","paper")>] HitResult() =
        member __.``type`` with get(): string = jsNative and set(v: string): unit = jsNative
        member __.name with get(): string = jsNative and set(v: string): unit = jsNative
        member __.item with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.location with get(): CurveLocation = jsNative and set(v: CurveLocation): unit = jsNative
        member __.color with get(): Color = jsNative and set(v: Color): unit = jsNative
        member __.segment with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.point with get(): Point = jsNative and set(v: Point): unit = jsNative

    and [<AllowNullLiteral>] [<Import("PathItem","paper")>] PathItem() =
        inherit Item()
        member __.pathData with get(): string = jsNative and set(v: string): unit = jsNative
        member __.getIntersections(path: PathItem, ?sorted: bool): ResizeArray<CurveLocation> = jsNative
        member __.smooth(): unit = jsNative
        member __.moveTo(point: Point): unit = jsNative
        member __.lineTo(point: Point): unit = jsNative
        member __.cubicCurveTo(handle1: Point, handle2: Point, ``to``: Point): unit = jsNative
        member __.quadraticCurveTo(handle: Point, ``to``: Point): unit = jsNative
        member __.curveTo(through: Point, ``to``: Point, ?parameter: float): unit = jsNative
        member __.arcTo(through: Point, ``to``: Point): unit = jsNative
        member __.arcTo(``to``: Point, ?clockwise: bool): unit = jsNative
        member __.closePath(join: bool): unit = jsNative
        member __.moveBy(``to``: Point): unit = jsNative
        member __.lineBy(``to``: Point): unit = jsNative
        member __.curveBy(through: Point, ``to``: Point, ?parameter: float): unit = jsNative
        member __.cubicCurveBy(handle1: Point, handle2: Point, ``to``: Point): unit = jsNative
        member __.quadraticCurveBy(handle: Point, ``to``: Point): unit = jsNative
        member __.arcBy(through: Point, ``to``: Point): unit = jsNative
        member __.arcBy(``to``: Point, ?clockwise: bool): unit = jsNative
        member __.unite(path: PathItem): PathItem = jsNative
        member __.intersect(path: PathItem): PathItem = jsNative
        member __.subtract(path: PathItem): PathItem = jsNative
        member __.exclude(path: PathItem): PathItem = jsNative
        member __.divide(path: PathItem): PathItem = jsNative

    and [<AllowNullLiteral>] [<Import("Path","paper")>] Path(?pathData: string) =
        inherit PathItem()
        member __.segments with get(): ResizeArray<Segment> = jsNative and set(v: ResizeArray<Segment>): unit = jsNative
        member __.firstSegment with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.lastSegment with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.curves with get(): ResizeArray<Curve> = jsNative and set(v: ResizeArray<Curve>): unit = jsNative
        member __.firstCurve with get(): Curve = jsNative and set(v: Curve): unit = jsNative
        member __.lastCurve with get(): Curve = jsNative and set(v: Curve): unit = jsNative
        member __.closed with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.length with get(): float = jsNative and set(v: float): unit = jsNative
        member __.area with get(): float = jsNative and set(v: float): unit = jsNative
        member __.fullySelected with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.clockwise with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.interiorPoint with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.add(segment: U2<Segment, Point>): Segment = jsNative
        member __.insert(index: float, segment: U2<Segment, Point>): Segment = jsNative
        member __.addSegments(segments: ResizeArray<Segment>): ResizeArray<Segment> = jsNative
        member __.insertSegments(index: float, segments: ResizeArray<Segment>): ResizeArray<Segment> = jsNative
        member __.removeSegment(index: float): Segment = jsNative
        member __.removeSegments(): ResizeArray<Segment> = jsNative
        member __.removeSegments(from: float, ?``to``: float): ResizeArray<Segment> = jsNative
        member __.flatten(maxDistance: float): unit = jsNative
        member __.simplify(?tolerance: float): unit = jsNative
        member __.split(offset: float): Path = jsNative
        member __.split(location: CurveLocation): Path = jsNative
        member __.split(index: float, parameter: float): Path = jsNative
        member __.reverse(): unit = jsNative
        member __.join(path: Path): Path = jsNative
        member __.getLocationOf(point: Point): CurveLocation = jsNative
        member __.getOffsetOf(point: Point): float = jsNative
        member __.getLocationAt(offset: float, ?isParameter: bool): CurveLocation = jsNative
        member __.getPointAt(offset: float, ?isPatameter: bool): Point = jsNative
        member __.getTangentAt(offset: float, ?isPatameter: bool): Point = jsNative
        member __.getNormalAt(offset: float, ?isParameter: bool): Point = jsNative
        member __.getCurvatureAt(offset: float, ?isParameter: bool, ?point: Point): float = jsNative
        member __.getNearestPoint(point: Point): Point = jsNative

    and [<AllowNullLiteral>] [<Import("CompoundPath","paper")>] CompoundPath(pathData: string) =
        inherit PathItem()
        member __.clockwise with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.firstSegment with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.lastSegment with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.curves with get(): ResizeArray<Curve> = jsNative and set(v: ResizeArray<Curve>): unit = jsNative
        member __.firstCurve with get(): Curve = jsNative and set(v: Curve): unit = jsNative
        member __.lastCurve with get(): Curve = jsNative and set(v: Curve): unit = jsNative
        member __.area with get(): float = jsNative and set(v: float): unit = jsNative
        member __.reverse(): unit = jsNative

    and [<AllowNullLiteral>] [<Import("Segment","paper")>] Segment(?``object``: obj) =
        member __.point with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.handleIn with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.handleOut with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.linear with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.selected with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.index with get(): float = jsNative and set(v: float): unit = jsNative
        member __.path with get(): Path = jsNative and set(v: Path): unit = jsNative
        member __.curve with get(): Curve = jsNative and set(v: Curve): unit = jsNative
        member __.location with get(): CurveLocation = jsNative and set(v: CurveLocation): unit = jsNative
        member __.next with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.previous with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.isColinear(segment: Segment): bool = jsNative
        member __.isArc(): bool = jsNative
        member __.reverse(): Segment = jsNative
        member __.remove(): bool = jsNative
        member __.toString(): string = jsNative
        member __.transform(matrix: Matrix): unit = jsNative

    and [<AllowNullLiteral>] [<Import("Curve","paper")>] Curve(point1: Point, handle1: Point, handle2: Point, point2: Point) =
        member __.point1 with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.point2 with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.handle1 with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.handle2 with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.segment1 with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.segment2 with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.path with get(): Path = jsNative and set(v: Path): unit = jsNative
        member __.index with get(): float = jsNative and set(v: float): unit = jsNative
        member __.next with get(): Curve = jsNative and set(v: Curve): unit = jsNative
        member __.previous with get(): Curve = jsNative and set(v: Curve): unit = jsNative
        member __.selected with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.length with get(): float = jsNative and set(v: float): unit = jsNative
        member __.bounds with get(): Rectangle = jsNative and set(v: Rectangle): unit = jsNative
        member __.strokeBounds with get(): Rectangle = jsNative and set(v: Rectangle): unit = jsNative
        member __.handleBounds with get(): Rectangle = jsNative and set(v: Rectangle): unit = jsNative
        member __.isLinear(): bool = jsNative
        member __.divide(?offset: float, ?isParameter: bool): Curve = jsNative
        member __.split(?offset: float, ?isParameter: bool): Path = jsNative
        member __.reverse(): Curve = jsNative
        member __.remove(): bool = jsNative
        member __.clone(): Curve = jsNative
        member __.toString(): string = jsNative
        member __.getParameterAt(offset: Point, ?start: float): float = jsNative
        member __.getParameterOf(point: Point): float = jsNative
        member __.getLocationAt(offset: Point, ?isParameter: bool): CurveLocation = jsNative
        member __.getLocationOf(point: Point): CurveLocation = jsNative
        member __.getOffsetOf(point: Point): float = jsNative
        member __.getPointAt(offset: float, ?isParameter: bool): Point = jsNative
        member __.getTangentAt(offset: float, ?isParameter: bool): Point = jsNative
        member __.getNormalAt(offset: float, ?isParameter: bool): Point = jsNative
        member __.getCurvatureAt(offset: float, ?isParameter: bool): Point = jsNative

    and [<AllowNullLiteral>] [<Import("CurveLocation","paper")>] CurveLocation(curve: Curve, parameter: float, point: Point) =
        member __.segment with get(): Segment = jsNative and set(v: Segment): unit = jsNative
        member __.curve with get(): Curve = jsNative and set(v: Curve): unit = jsNative
        member __.intersection with get(): CurveLocation = jsNative and set(v: CurveLocation): unit = jsNative
        member __.path with get(): Path = jsNative and set(v: Path): unit = jsNative
        member __.index with get(): float = jsNative and set(v: float): unit = jsNative
        member __.offset with get(): float = jsNative and set(v: float): unit = jsNative
        member __.curveOffset with get(): float = jsNative and set(v: float): unit = jsNative
        member __.parameter with get(): float = jsNative and set(v: float): unit = jsNative
        member __.point with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.distance with get(): float = jsNative and set(v: float): unit = jsNative
        member __.equals(location: CurveLocation): bool = jsNative
        member __.toString(): string = jsNative

    and [<AllowNullLiteral>] [<Import("Project","paper")>] Project(element: U2<HTMLCanvasElement, string>) =
        member __.view with get(): View = jsNative and set(v: View): unit = jsNative
        member __.currentStyle with get(): Style = jsNative and set(v: Style): unit = jsNative
        member __.index with get(): float = jsNative and set(v: float): unit = jsNative
        member __.layers with get(): ResizeArray<Layer> = jsNative and set(v: ResizeArray<Layer>): unit = jsNative
        member __.activeLayer with get(): Layer = jsNative and set(v: Layer): unit = jsNative
        member __.symbols with get(): ResizeArray<Symbol> = jsNative and set(v: ResizeArray<Symbol>): unit = jsNative
        member __.activate(): unit = jsNative
        member __.clear(): unit = jsNative
        member __.isEmpty(): bool = jsNative
        member __.remove(): unit = jsNative
        member __.selectAll(): unit = jsNative
        member __.deselectAll(): unit = jsNative
        member __.hitTest(point: Point, ?options: obj): HitResult = jsNative
        member __.getItems(``match``: obj): ResizeArray<Item> = jsNative
        member __.getItem(``match``: obj): Item = jsNative
        member __.exportJSON(?options: obj): string = jsNative
        member __.importJSON(json: string): unit = jsNative
        member __.exportSVG(?options: obj): SVGElement = jsNative
        member __.importSVG(svg: U2<SVGElement, string>, ?options: obj): Item = jsNative

    and [<AllowNullLiteral>] [<Import("Symbol","paper")>] Symbol(item: Item, ?dontCenter: bool) =
        member __.project with get(): Project = jsNative and set(v: Project): unit = jsNative
        member __.definition with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.place(?position: Point): PlacedSymbol = jsNative
        member __.clone(): Symbol = jsNative

    and [<AllowNullLiteral>] [<Import("Style","paper")>] Style() =
        member __.view with get(): View = jsNative and set(v: View): unit = jsNative
        member __.strokeColor with get(): U2<Color, string> = jsNative and set(v: U2<Color, string>): unit = jsNative
        member __.strokeWidth with get(): float = jsNative and set(v: float): unit = jsNative
        member __.strokeCap with get(): string = jsNative and set(v: string): unit = jsNative
        member __.strokeJoin with get(): string = jsNative and set(v: string): unit = jsNative
        member __.strokeScaling with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.dashOffset with get(): float = jsNative and set(v: float): unit = jsNative
        member __.dashArray with get(): ResizeArray<float> = jsNative and set(v: ResizeArray<float>): unit = jsNative
        member __.miterLimit with get(): float = jsNative and set(v: float): unit = jsNative
        member __.fillColor with get(): U2<Color, string> = jsNative and set(v: U2<Color, string>): unit = jsNative
        member __.shadowColor with get(): U2<Color, string> = jsNative and set(v: U2<Color, string>): unit = jsNative
        member __.shadowBlur with get(): float = jsNative and set(v: float): unit = jsNative
        member __.shadowOffset with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.selectedColor with get(): U2<Color, string> = jsNative and set(v: U2<Color, string>): unit = jsNative
        member __.fontFamily with get(): string = jsNative and set(v: string): unit = jsNative
        member __.fontWeight with get(): U2<string, float> = jsNative and set(v: U2<string, float>): unit = jsNative
        member __.fontSize with get(): U2<string, float> = jsNative and set(v: U2<string, float>): unit = jsNative
        member __.leading with get(): U2<float, string> = jsNative and set(v: U2<float, string>): unit = jsNative
        member __.justification with get(): string = jsNative and set(v: string): unit = jsNative

    and [<AllowNullLiteral>] IHSBColor =
        abstract hue: float option with get, set
        abstract saturation: float option with get, set
        abstract brightness: float option with get, set
        abstract alpha: float option with get, set

    and [<AllowNullLiteral>] IHSLColor =
        abstract hue: float option with get, set
        abstract saturation: float option with get, set
        abstract lightness: float option with get, set
        abstract alpha: float option with get, set

    and [<AllowNullLiteral>] IGradientColor =
        abstract gradient: Gradient option with get, set
        abstract origin: Point option with get, set
        abstract destination: Point option with get, set
        abstract radial: bool option with get, set

    and [<AllowNullLiteral>] [<Import("Color","paper")>] Color(hex: string) =
        member __.``type`` with get(): string = jsNative and set(v: string): unit = jsNative
        member __.components with get(): float = jsNative and set(v: float): unit = jsNative
        member __.alpha with get(): float = jsNative and set(v: float): unit = jsNative
        member __.red with get(): float = jsNative and set(v: float): unit = jsNative
        member __.green with get(): float = jsNative and set(v: float): unit = jsNative
        member __.blue with get(): float = jsNative and set(v: float): unit = jsNative
        member __.gray with get(): float = jsNative and set(v: float): unit = jsNative
        member __.hue with get(): float = jsNative and set(v: float): unit = jsNative
        member __.saturation with get(): float = jsNative and set(v: float): unit = jsNative
        member __.brightness with get(): float = jsNative and set(v: float): unit = jsNative
        member __.lightness with get(): float = jsNative and set(v: float): unit = jsNative
        member __.gradient with get(): Gradient = jsNative and set(v: Gradient): unit = jsNative
        member __.highlight with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.convert(``type``: string): Color = jsNative
        member __.hasAlpha(): bool = jsNative
        member __.equals(color: Color): bool = jsNative
        member __.clone(): Color = jsNative
        member __.toString(): string = jsNative
        member __.toCSS(hex: bool): string = jsNative
        member __.transform(matrix: Matrix): unit = jsNative

    and [<AllowNullLiteral>] [<Import("Gradient","paper")>] Gradient() =
        member __.stops with get(): ResizeArray<GradientStop> = jsNative and set(v: ResizeArray<GradientStop>): unit = jsNative
        member __.radial with get(): bool = jsNative and set(v: bool): unit = jsNative
        member __.clone(): Gradient = jsNative
        member __.equals(gradient: Gradient): bool = jsNative

    and [<AllowNullLiteral>] [<Import("GradientStop","paper")>] GradientStop(?color: Color, ?rampPoint: float) =
        member __.rampPoint with get(): float = jsNative and set(v: float): unit = jsNative
        member __.color with get(): Color = jsNative and set(v: Color): unit = jsNative
        member __.clone(): GradientStop = jsNative

    and [<AllowNullLiteral>] [<Import("View","paper")>] View() =
        member __.element with get(): HTMLCanvasElement = jsNative and set(v: HTMLCanvasElement): unit = jsNative
        member __.pixelRatio with get(): float = jsNative and set(v: float): unit = jsNative
        member __.resolution with get(): float = jsNative and set(v: float): unit = jsNative
        member __.viewSize with get(): Size = jsNative and set(v: Size): unit = jsNative
        member __.bounds with get(): Rectangle = jsNative and set(v: Rectangle): unit = jsNative
        member __.size with get(): Size = jsNative and set(v: Size): unit = jsNative
        member __.center with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.zoom with get(): float = jsNative and set(v: float): unit = jsNative
        member __.onFrame with get(): Func<IFrameEvent, unit> = jsNative and set(v: Func<IFrameEvent, unit>): unit = jsNative
        member __.onResize with get(): Func<Event, unit> = jsNative and set(v: Func<Event, unit>): unit = jsNative
        member __.remove(): unit = jsNative
        member __.isVisible(): bool = jsNative
        member __.scrollBy(point: Point): unit = jsNative
        member __.play(): unit = jsNative
        member __.pause(): unit = jsNative
        member __.update(): unit = jsNative
        member __.projectToView(point: Point): Point = jsNative
        member __.viewToProject(point: Point): Point = jsNative
        member __.on(``type``: string, callback: Func<Event, unit>): Item = jsNative
        member __.on(param: obj): Item = jsNative
        member __.off(``type``: string, callback: Func<Event, unit>): Item = jsNative
        member __.off(param: obj): Item = jsNative
        member __.emit(``type``: string, ``event``: obj): bool = jsNative
        member __.responds(``type``: string): bool = jsNative
        member __.draw(): unit = jsNative

    and [<AllowNullLiteral>] [<Import("Tool","paper")>] Tool() =
        member __.minDistance with get(): float = jsNative and set(v: float): unit = jsNative
        member __.maxDistance with get(): float = jsNative and set(v: float): unit = jsNative
        member __.fixedDistance with get(): float = jsNative and set(v: float): unit = jsNative
        member __.onMouseDown with get(): Func<ToolEvent, unit> = jsNative and set(v: Func<ToolEvent, unit>): unit = jsNative
        member __.onMouseDrag with get(): Func<ToolEvent, unit> = jsNative and set(v: Func<ToolEvent, unit>): unit = jsNative
        member __.onMouseMove with get(): Func<ToolEvent, unit> = jsNative and set(v: Func<ToolEvent, unit>): unit = jsNative
        member __.onMouseUp with get(): Func<ToolEvent, unit> = jsNative and set(v: Func<ToolEvent, unit>): unit = jsNative
        member __.onKeyDown with get(): Func<KeyEvent, unit> = jsNative and set(v: Func<KeyEvent, unit>): unit = jsNative
        member __.onKeyUp with get(): Func<KeyEvent, unit> = jsNative and set(v: Func<KeyEvent, unit>): unit = jsNative
        member __.activate(): unit = jsNative
        member __.remove(): unit = jsNative
        member __.on(``type``: string, callback: Func<ToolEvent, unit>): Tool = jsNative
        member __.on(param: obj): Tool = jsNative
        member __.off(``type``: string, callback: Func<ToolEvent, unit>): Tool = jsNative
        member __.off(param: obj): Tool = jsNative
        member __.emit(``type``: string, ``event``: obj): bool = jsNative
        member __.responds(``type``: string): bool = jsNative

    and [<AllowNullLiteral>] [<Import("Event","paper")>] Event() =
        member __.modifiers with get(): obj = jsNative and set(v: obj): unit = jsNative

    and [<AllowNullLiteral>] [<Import("ToolEvent","paper")>] ToolEvent() =
        inherit Event()
        member __.``type`` with get(): string = jsNative and set(v: string): unit = jsNative
        member __.point with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.lastPoint with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.downPoint with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.middlePoint with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.delta with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.count with get(): float = jsNative and set(v: float): unit = jsNative
        member __.item with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.toString(): string = jsNative

    and [<AllowNullLiteral>] [<Import("Key","paper")>] Key() =
        static member isDown(key: string): bool = jsNative

    and [<AllowNullLiteral>] [<Import("KeyEvent","paper")>] KeyEvent() =
        inherit Event()
        member __.``type`` with get(): string = jsNative and set(v: string): unit = jsNative
        member __.character with get(): string = jsNative and set(v: string): unit = jsNative
        member __.key with get(): string = jsNative and set(v: string): unit = jsNative
        member __.toString(): string = jsNative

    and [<AllowNullLiteral>] [<Import("TextItem","paper")>] TextItem() =
        inherit Item()
        member __.content with get(): string = jsNative and set(v: string): unit = jsNative
        member __.fontFamily with get(): string = jsNative and set(v: string): unit = jsNative
        member __.fontWeight with get(): U2<string, float> = jsNative and set(v: U2<string, float>): unit = jsNative
        member __.fontSize with get(): U2<string, float> = jsNative and set(v: U2<string, float>): unit = jsNative
        member __.leading with get(): U2<string, float> = jsNative and set(v: U2<string, float>): unit = jsNative
        member __.justification with get(): string = jsNative and set(v: string): unit = jsNative

    and [<AllowNullLiteral>] [<Import("PointText","paper")>] PointText(``object``: obj) =
        inherit TextItem()
        member __.point with get(): Point = jsNative and set(v: Point): unit = jsNative

    and [<AllowNullLiteral>] [<Import("MouseEvent","paper")>] MouseEvent(``type``: string, ``event``: NativeMouseEvent, point: Point, target: Item, delta: Point) =
        inherit Event()
        member __.``event`` with get(): NativeMouseEvent = jsNative and set(v: NativeMouseEvent): unit = jsNative
        member __.point with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.lastPoint with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.delta with get(): Point = jsNative and set(v: Point): unit = jsNative
        member __.target with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.currentTarget with get(): Item = jsNative and set(v: Item): unit = jsNative
        member __.``type`` with get(): (* TODO StringEnum mousedown | mouseup | mousedrag | click | doubleclick | mousemove | mouseenter | mouseleave *) string = jsNative and set(v: (* TODO StringEnum mousedown | mouseup | mousedrag | click | doubleclick | mousemove | mouseenter | mouseleave *) string): unit = jsNative
        member __.timeStamp(): float = jsNative
        member __.preventDefault(): unit = jsNative
        member __.stopPropagation(): unit = jsNative
        member __.stop(): unit = jsNative

    type [<Import("*","paper")>] Globals =
        static member version with get(): string = jsNative and set(v: string): unit = jsNative
        static member settings with get(): settingsType = jsNative and set(v: settingsType): unit = jsNative
        static member project with get(): Project = jsNative and set(v: Project): unit = jsNative
        static member projects with get(): ResizeArray<Project> = jsNative and set(v: ResizeArray<Project>): unit = jsNative
        static member view with get(): View = jsNative and set(v: View): unit = jsNative
        static member tool with get(): Tool = jsNative and set(v: Tool): unit = jsNative
        static member tools with get(): ResizeArray<Tool> = jsNative and set(v: ResizeArray<Tool>): unit = jsNative
        static member install(scope: obj): unit = jsNative
        static member setup(canvas: U2<HTMLCanvasElement, string>): unit = jsNative
        static member activate(): unit = jsNative

    module Path =
        type [<AllowNullLiteral>] [<Import("Path.Line","paper")>] Line(``object``: obj) =
            inherit Path()


        and [<AllowNullLiteral>] [<Import("Path.Circle","paper")>] Circle(``object``: obj) =
            inherit Path()



        and [<AllowNullLiteral>] [<Import("Path.Rectangle","paper")>] Rectangle(``object``: obj) =
            inherit Path()



        and [<AllowNullLiteral>] [<Import("Path.Ellipse","paper")>] Ellipse(``object``: obj) =
            inherit Path()


        and [<AllowNullLiteral>] [<Import("Path.Arc","paper")>] Arc(``object``: obj) =
            inherit Path()


        and [<AllowNullLiteral>] [<Import("Path.RegularPolygon","paper")>] RegularPolygon(``object``: obj) =
            inherit Path()


        and [<AllowNullLiteral>] [<Import("Path.Star","paper")>] Star(``object``: obj) =
            inherit Path()
