module Annotate.World.WebApp.Components.Searchbar

open Fable.Import.Browser
open Fable.Import.React
open Fable.Import.ReactDom
open Fable.Core.JsInterop
open Fable.Core
open Fable.Import.Leaflet

open Annotate.World.WebApp.Styles

module R = Fable.Helpers.React
open R.Props

type SearchbarProps = { currentText:string; onChange: string->unit }

let Searchbar (props: SearchbarProps) =
    R.input [ 
        Style  ( borderStyle @ headerStyle @ [ 
                    Color annotateBlue
                    Position "absolute" 
                    Left 40
                    Top 40
                    ZIndex 1000.0
                    Padding "10px"
                    CSSProp.Width "600px"
                    CSSProp.Height "32px"
                    CSSProp.FontFamily "Alma Mono, monospace"
                    CSSProp.FontSize !^24.0
                    Padding 10
                    FontWeight !^"bold"
                    BackgroundImage "url(/img/blue-icons/search-icon.svg)"
                    BackgroundRepeat "no-repeat"                
                    BackgroundPositionX (600-32)
                    BackgroundOrigin "content-box"                
               ])
        Placeholder "search..."; 
        OnChange (fun evt -> props.onChange <| string evt.target?value)
        Value !^props.currentText
    ] 


