module Annotate.World.WebApp.Components.ToolSelection

open Fable.Import.Browser
open Fable.Import.React
open Fable.Import.ReactDom
open Fable.Core.JsInterop
open Fable.Core
open Fable.Import.Leaflet
module R = Fable.Helpers.React
open R.Props

open Annotate.World.Annotations
open Annotate.World.WebApp.Styles
open Annotate.World.WebApp.Components.Spacer
open Annotate.World.WebApp.Domain.Tools



type ToolSelectorEntryProp = { label:string; icon:string; tool: Tool }

type ToolSelectorProps = { currentlySelectedTool: Tool; color : RGBColor; onToolSelect: (Tool -> unit) }


let ToolEntry  (props: ToolSelectorProps) (entry: ToolSelectorEntryProp) = 
    let (fgColor, backgroundColor, iconPath) = 
      if props.currentlySelectedTool = entry.tool then 
        ("#FFF", toHex props.color, sprintf "./img/white-icons/%s-icon.svg" entry.icon)
      else 
        (annotateBlue, "transparent", sprintf "./img/blue-icons/%s-icon.svg" entry.icon)
                    
                    
     
    let htmlProps: IHTMLProp list = 
      [ 
        ClassName "clickable unselectable"
        OnClick (fun _ -> props.onToolSelect entry.tool) 
        Style ( roundedBorderStyle @ 
                [ Flex !^1. 
                  Display "flex" 
                  PaddingBottom standardMargin 
                  BackgroundColor backgroundColor                  
                ] 
              )        
      ]

    R.div htmlProps
          [
            Spacer            
            R.div [Style (textStyle @ [Margin 0; Padding 0.; CSSProp.FontSize (!^12.0);]) ] 
                  [
                    R.img [Src iconPath ;Style [CSSProp.Width "48px"; Height "48px"; Padding "8px" ]] 
                    R.div [Style [TextAlign "center"; Color fgColor]]  [R.str entry.label] 
                  ]
            Spacer
          ]

let ToolSelectorContainer (props: ToolSelectorProps) (tools:ToolSelectorEntryProp seq)= 
    R.div [Style [Display "flex"; FlexWrap "wrap"; Margin standardMargin]] 
          ((tools |> Seq.map (ToolEntry props) |> Seq.toList) @ [Spacer]) // Add spacer at end to align last row to left
  
let ToolSelector (props: ToolSelectorProps) =
    ToolSelectorContainer (props) 
                          [ { label = "select"; icon = "select"; tool = SelectTool }
                            { label = "ruler"; icon = "ruler"; tool = RulerTool }                              
                            { label = "point"; icon = "point"; tool = PointTool }                              
                            { label = "line";  icon = "line"; tool = LineTool }                              
                            { label = "shape"; icon = "shape"; tool = ShapeTool }                              
                            { label = "rect";  icon = "rect"; tool = RectangleTool }                              
                            { label = "circ";  icon = "circ"; tool = CircleTool }                              
                          ]

