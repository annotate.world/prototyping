module Annotate.World.WebApp.Components.Leaflet

open Fable.Import.Browser
open Fable.Import.React
open Fable.Import.ReactDom
open Fable.Core.JsInterop
open Fable.Core
open Fable.Import.Leaflet

open Fable.Import.Paper
open Annotate.World.WebApp.Components.MapOverlay

open Fable.Import

open Annotate.World.Geometry

module R = Fable.Helpers.React
open R.Props


type [<Pojo>] MapProps = { center:Point; zoom:float; overlays: ID3MapOverlay list }
type [<Pojo>] MapState = { center:Point; zoom:float; map:Map option }

type private MapComponent (props:MapProps) =
    inherit Fable.Import.React.Component<MapProps,MapState>(props)
                
    let initialState = { center = props.center
                         zoom = props.zoom
                         map = None                       
                       }
    let baseLayerTileUrl = "https://api.mapbox.com/styles/v1/mapbox/outdoors-v10/tiles/256/{z}/{x}/{y}?access_token={accessToken}"                           
    let baseLayerOptions = createObj [ "id" ==> Some "mapbox.streets" 
                                       "accessToken" ==> (Some "pk.eyJ1IjoiaHVycmljYW5lc2xpZGVyIiwiYSI6IlZZZHNEdU0ifQ.aKmndwaat-Ri-bNkifcVJA")
                                     ]                        
    let baseLayer = TileLayer(baseLayerTileUrl, baseLayerOptions :?> TileLayerOptions) :> Leaflet.Layer                                     
    let southWestBound = (-88.98155760646617, 180.)
    let northEastBound = (88.99346179538875, -180.)    
    let maxBounds = Leaflet.LatLngBounds(System.Collections.Generic.List [southWestBound;northEastBound])
    let mapOptions = createObj [ "zoomControl" ==> Some false; 
                                 "attributionControl" ==> Some false 
                                 "maxZoom" ==> Some 18. 
                                 "minZoom" ==> Some 2.5                                
                                 "maxBounds" ==> Some maxBounds
                                 "maxBoundsViscosity" ==> Some 0.95
                               ] :?> MapOptions                        
    
    do base.setInitState(initialState)    
    
    member this.setView (map:Map) =         
        let center = (this.state.center.y*1.</deg>, this.state.center.x*1.</deg>)
        map.setView(!^center, this.state.zoom) |> ignore        
        ()
        
    member this.createMapIfNotExists (element: Element) =                 
        match this.state.map with
        | None -> 
            let htmlEl = element :?> HTMLElement            
            let map = Map(!^htmlEl, mapOptions)         
            
            this.setState({ this.state with map = Some map})
            this.setView map |> ignore

            let pane = map.createPane("annotation-pane")                    
            let canvas = pane.ownerDocument.createElement_canvas()            
            let cz = map.latLngToLayerPoint(!^(90.,-180.))
            let c1 = map.latLngToLayerPoint(!^(-90., 180.))   
            let pixelBounds = map.getPixelBounds()
            canvas.width <- abs pixelBounds.max.x
            canvas.height <- abs pixelBounds.max.y
            pane.appendChild(canvas) |> ignore

            Paper.Globals.setup(!^canvas) |> ignore
            let render () =                 
                let path = Paper.Path()             
                (path :> Item).strokeColor <- !^"red"
                (path :> Item).strokeWidth <- 20.
                let offset = map.getPixelOrigin()
                let pt = map.latLngToLayerPoint(!^(0., 0.))
                let pt2 = map.latLngToLayerPoint(!^(50., 0.))
                printfn "%A" pt
                printfn "%A" pt2
                let start = Paper.Point(1920./2.,1080./2.)
                path.moveTo(start) |> ignore
                path.lineTo(start.add(ResizeArray [0. ; 1080./2. ])) 
                Paper.Globals.view.draw() |> ignore
                baseLayer.addTo(map) |> ignore     
            map.on("zoomstart", fun _ -> Paper.Globals.project.clear()) |> ignore
            map.on("zoomend", fun _ -> render()) |> ignore
            map.on("zoomend", fun _ -> render()) |> ignore
            map.on("moveend", fun _ -> render()) |> ignore
            render() |> ignore
            //Todo port this code: https://github.com/teeg82/paperjs_leaflet_test/blob/master/js/PaperManager.js   
            ()                       
        | Some map -> ()        
        
    member this.componentWillReceiveProps nextProps =        
        if (nextProps.center <> this.state.center) ||  (nextProps.zoom <> this.state.zoom) then 
            this.setState ( { this.state with
                                center = nextProps.center                                
                                zoom = nextProps.zoom
                            }
                          )               
            match this.state.map with
            | Some map -> this.setView map
            | None -> ()                                          
        else
            ()        

    member this.render() =                 
        R.div [Prop.Ref this.createMapIfNotExists; Style [CSSProp.Width "100%"; Height "100%"] ] []

let Map (props:MapProps)=    
    R.com<MapComponent,_,_> props []
    


 
