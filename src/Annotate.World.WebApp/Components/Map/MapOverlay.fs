module Annotate.World.WebApp.Components.MapOverlay

open Fable.Core

open Fable.Core.JsInterop
open Fable.Import.Leaflet
open Fable.Import
open Fable.Import.Browser
open Fable.Import.D3
open Fable.Import.D3.Geo

module R = Fable.Helpers.React
open R.Props


type [<Pojo>] D3MapOverlayState = { svg: D3.Selection<SVGSVGElement>; g: D3.Selection<SVGGElement>; pane: HTMLElement; map: Leaflet.Map }


type ID3MapOverlay = 
   abstract member bounds : unit -> float*float*float*float        
   abstract member reproject : unit -> unit
   abstract member create : unit -> unit
   abstract member update : unit -> unit
   abstract member destroy : unit -> unit
   abstract member onMapCreated : Leaflet.Map -> unit

[<AbstractClass>]
type D3MapOverlay<[<Pojo>]'Props> (props: 'Props) as this =           
        let mutable _internalState: D3MapOverlayState option = None
        let mutable _props = props

        abstract member bounds : unit -> float*float*float*float
        abstract member reproject : unit -> unit
        abstract member create : unit -> unit
        abstract member update : unit -> unit
        abstract member destroy : unit -> unit       
        
        member this.state 
            with get () = 
                match _internalState with
                | Some state -> state
                | None -> failwithf "State has not been initialized"
            and set (v) =                 
                _internalState <- Some v
                this.update()

        member this.props 
            with get () = props
            and set (p) = _props <- p                

        interface ID3MapOverlay with
            member x.bounds () = this.bounds ()
            member x.reproject() = this.reproject()
            member x.create () = this.create()
            member x.update () = this.update()
            member x.destroy () = this.destroy ()
            
            member x.onMapCreated (map : Leaflet.Map) =                    
                let pane = map.createPane(System.Guid.NewGuid() |> string)
                let svg =  D3.Globals.select(pane).append("svg") :?> Selection<SVGSVGElement>
                let g = svg.append("g") :?> Selection<SVGGElement>
                g.attr("class", "leaflet-zoom-hide") |> ignore 
                
                let initState = { svg = svg; pane = pane; g = g; map = map }            
                _internalState <- Some initState
                this.create()                 
                this.update() 
                this.state.map.on("viewreset", fun e-> this.resetView()) |> ignore
                this.state.map.on("zoom", fun e -> this.resetView()) |> ignore
                this.resetView()  

        member this.projectPoint ((x,y):float*float) =                         
            let coord = (y,x)                            
            let pt = this.state.map.latLngToLayerPoint(!^coord)
            (pt.x, pt.y)                
        
        member this.resetView () =          
            let (topLeftX, topLeftY, bottomRightX, bottomRightY) = this.bounds()
            
            this.state.svg.attr("width", sprintf "%fpx" (abs (bottomRightX - topLeftX)))
                          .attr("height",  sprintf "%fpx" (abs (bottomRightY - topLeftY)))
                          .style("left", sprintf "%fpx" topLeftX)
                          .style("top", sprintf "%fpx" topLeftY)  |> ignore            
                          
            this.state.g.attr("transform", sprintf "translate(%f,%f)" (-topLeftX) (-topLeftY)) |> ignore
            this.reproject()



        



type [<Pojo>] CircleProps = { circles: (float*float) array }

type CircleOverlay (props: CircleProps) = 
    inherit D3MapOverlay<CircleProps> (props)                
        override this.bounds () =                         
            let path = D3.Geo.Globals.path().projection(this.projectPoint)
            let bounds = path.bounds(this.props.circles)
            (bounds.[0].[0],bounds.[0].[1], bounds.[1].[0], bounds.[1].[1])
            
        override this.reproject () = 
            
            //this.state.g.selectAll("circle").data(props.circles) |> ignore
            this.update()
        
        override this.destroy () = ()            

        override this.create() = 
            let circles = this.state.g.selectAll("circle").data(props.circles)

            circles?enter()?append("circle")?attr("r", "100") |> ignore            
            circles?exit()?remove() |> ignore

        override this.update () = 
            let circles  = this.state.g.selectAll("circle").data(this.props.circles) 
            circles?attr("cx", this.projectPoint >> fst)
                   ?attr("cy", this.projectPoint >> snd)
            |> ignore


let MapCircleOverlay (props:CircleProps) = new CircleOverlay(props)




// create: D3Func<'Props>    
// update: D3Func<'Props>     
// destroy: D3Func<'Props>
// reproject: D3ProjectionFunc<'Props> 
// bounds:  Leaflet.Map -> Selection<Browser.SVGGElement> -> 'Props -> (float*float*float*float)

// type D3MapOverlay2<[<Pojo>]'Props,[<Pojo>] 'State> (map: Leaflet.Map, pane: HTMLElement, calculateBounds, props: 'Props) =     
//     inherit Fable.Import.React.Component<'Props,'State>(props)
//         member this.componentWillMount () =
//             map.on("viewreset", fun e-> this.resetView()) |> ignore
//             map.on("zoom", fun e -> this.resetView()) |> ignore    

//         member this.projectPoint ((x,y):float*float)  =                         
//             let coord = (y,x)                
//             let pt = map.latLngToLayerPoint(!^coord)
//             (pt.x, pt.y)

//         member this.svg = D3.Globals.select(pane).append("svg")
//         member this.g = this.svg.append("g").attr("class", "leaflet-zoom-hide")

//         member this.resetView () = 
//             let (topLeftX, topLeftY, bottomRightX, bottomRightY) = calculateBounds this.state
//             // let path = D3.Geo.Globals.path().projection(projectPoint)
//             // let feature = g.selectAll("path")?data(collection?features)?enter()?append("path")                        
//             this.svg.attr("width", sprintf "%fpx" (bottomRightX - topLeftX))
//                     .attr("height",  sprintf "%fpx" (bottomRightY - topLeftY))
//                     .style("left", sprintf "%fpx" topLeftX)
//                     .style("top", sprintf "%fpx" topLeftY)  |> ignore
            
//             this.g.attr("transform", sprintf "translate(%f,%f)" (-topLeftX) (-topLeftY)) |> ignore
//             // feature?attr("d", path) |> ignore
        
        

// let inline reset (svg:D3.Selection<obj>) (g:D3.Selection<obj>) (path: D3.Geo.Path) feature collection =                                                 
//     let bounds = path.bounds(collection)                                  
//     let topLeftX = bounds.[0].[0]
//     let topLeftY = bounds.[0].[1]
//     let bottomRightX = bounds.[1].[0]
//     let bottomRightY = bounds.[1].[1]
    
//     svg.attr("width", sprintf "%fpx" (bottomRightX - topLeftX))
//        .attr("height",  sprintf "%fpx" (bottomRightY - topLeftY))
//        .style("left", sprintf "%fpx" topLeftX)
//        .style("top", sprintf "%fpx" topLeftY)  |> ignore

//     g.attr("transform", sprintf "translate(%f,%f)" (-topLeftX) (-topLeftY)) |> ignore
//     feature?attr("d", path) |> ignore

// let USOverlay (map: Leaflet.Map) (pane: HTMLElement) =
//     let svg = D3.Globals.select(pane).append("svg")
//     let g = svg.append("g").attr("class", "leaflet-zoom-hide")

//     let jsonResult error collection =        
//         if not <| isNull error then 
//             failwithf  "%A" error
//         else
//             let projectPoint ((x,y):float*float) =                            
//                 let coord = (y,x)
                
//                 let pt = map.latLngToLayerPoint(!^coord)
//                 (pt.x, pt.y)
                                                                                   
//             let path = D3.Geo.Globals.path().projection(projectPoint)
//             let feature = g.selectAll("path")?data(collection?features)?enter()?append("path")                        

//             let reset = fun () -> reset svg g path feature collection

//             map.on("viewreset", fun e-> reset() |> ignore) |> ignore
//             map.on("zoom", fun e -> reset() |> ignore) |> ignore                    
//             reset () |> ignore

//     D3.Globals.json("us-states.json", System.Func<obj,obj,unit> jsonResult) |> ignore
