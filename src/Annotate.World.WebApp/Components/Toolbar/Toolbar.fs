module Annotate.World.WebApp.Components.Toolbar
open Annotate.World.WebApp.Components.Spacer
open Fable.Import.Browser
open Fable.Import.React
open Fable.Import.ReactDom
open Fable.Core.JsInterop
open Fable.Core
open Fable.Import.Leaflet

open Annotate.World.WebApp.Styles

module R = Fable.Helpers.React
open R.Props


type ToolbarProps = { title:string; opened:bool; onVisiblityToggled: bool -> unit }

let private containerProps: IHTMLProp list = [ Style [ Position "absolute"
                                                       CSSProp.Height "100vh" 
                                                       CSSProp.Width "18vw"
                                                       Display "flex"                                                       
                                                       FlexDirection "column" 
                                                       Top 0
                                                       Bottom 0
                                                       Right 10
                                                       ZIndex 1001.
                                                       MarginTop 0
                                                       MarginBottom 0
                                                       PointerEvents "none"
                                                       MarginRight 10
                                                       MarginLeft 10
                                                       BackgroundColor "none"                                                       
                                                    ]
                                              ]

let ToolbarHeader (props: ToolbarProps) = 
  let onClick = OnClick (fun _-> props.onVisiblityToggled (not props.opened))

  [ 
    R.div [Style [Display "flex"; FlexDirection "row"]] [                
      R.h1 [ ClassName "clickable"; Style headerStyle; onClick] [R.str props.title] 
      Spacer
      R.h1 [ ClassName "clickable"; Style (headerStyle @ [TextAlign Right]); onClick] [R.str (if props.opened then "-" else "+" )] 
    ]
  ]

let Toolbar (props: ToolbarProps) contents = 
    R.div [Style (borderStyle @ [ CSSProp.Width "100%"; Margin 10; PointerEvents "auto" ])]
          ( 
            (ToolbarHeader props) @ (if props.opened then contents else [])
          )
          
let ToolbarContainer children =
    R.div containerProps (Spacer::children)

