module Annotate.World.WebApp.Components.Spacer
module R = Fable.Helpers.React
open R.Props

let Spacer = (R.div [Style [FlexGrow 1.0]] [])